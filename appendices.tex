%%% Attachments to the master thesis, if any. Each attachment must be
%%% referred to at least once from the text of the thesis. Attachments
%%% are numbered.
%%%
%%% The printed version should preferably contain attachments, which can be
%%% read (additional tables and charts, supplementary text, examples of
%%% program output, etc.). The electronic version is more suited for attachments
%%% which will likely be used in an electronic form rather than read (program
%%% source code, data files, interactive charts, etc.). Electronic attachments
%%% should be uploaded to SIS and optionally also included in the thesis on a~CD/DVD.
%%% Allowed file formats are specified in provision of the rector no. 72/2017.
\appendix

\chapter{Attachments}

The~thesis contains electronic attachments which are submitted in university study information system (SIS). The~attachments have structure which is shown in Figure~\ref{figure-attachments}. The~description of particular folders and subfolders follows. Please note that the~PCRF server is not part of the~electronic attachment.

\begin{figure}[htb]
\dirtree{%
 .1 root.
 .2 pcrf-mff.
 .3 disl.
 .4 config.
 .4 libs.
 .4 src.
 .3 plots.
 .4 out.
 .5 1529598143-constant-500.
 .5 1531611141-call-performance.
 .5 1531631495-big-event.
 .5 1531641149-real-life.
 .5 1531816106-stress-test.
 .3 real-scenarios.
 .4 config.
 .5 simple.
 .2 traffirator.
 .3 examples.
 .4 scripts.
 .3 logs.
 .3 src.
}
\caption{Folder Structure of Electronic Attachments}
\label{figure-attachments}
\end{figure}

\begin{itemize}
	\conciseitemize
	\item \texttt{pcrf-mff} -- Directory contains all scripts, sources and configuration for the~testing framework and alike.
	\begin{itemize}[topsep=0pt]
		\conciseitemize	
        \item \texttt{disl} -- Contains sources and configuration of the~DiSL instrumentation implementation.
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item \texttt{config} -- Contains configuration of the~MediationZone Execution Context which is used with DiSL.
            \item \texttt{libs} -- Folder which contains libraries needed for successful compilation of the~DiSL instrumentation.
            \item \texttt{src} -- Sources of the~DiSL instrumentation are stored in this folder.
        \end{itemize}
        \item \texttt{plots} -- Directory contains number of scripts used for plotting of the~graphs with the~use of R statistical software. There are also results from performed measurements.
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item \texttt{out} -- Contains results from performed measurements.
            \begin{itemize}[topsep=0pt]
                \conciseitemize
                \item \texttt{1529598143-constant-500} -- Results from the~constant profile which held 500 active scenarios.
                \item \texttt{1531611141-call-performance} -- Results from the~call performance test profile.
                \item \texttt{1531631495-big-event} -- Results from the~big event profile.
                \item \texttt{1531641149-real-life} -- Results from the~real-life profile.
                \item \texttt{1531816106-stress-test} -- Results from the~stress test profile.
            \end{itemize}
        \end{itemize}
        \item \texttt{real-scenarios} -- The~main content of this directory is execution script for the~testing framework and configuration needed for the~execution.
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item \texttt{config} -- Folder contains configuration used in the~testing framework.
            \begin{itemize}[topsep=0pt]
                \conciseitemize
                \item \texttt{simple} -- Contains simple scenarios for the~generator which can be used as a~proof of concept.
            \end{itemize}
        \end{itemize}
    \end{itemize}
	\item \texttt{traffirator} -- Root directory of traffic generator implementation.
    \begin{itemize}[topsep=0pt]
        \conciseitemize
        \item \texttt{examples} -- Contains some example configurations for the~generator.
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item \texttt{scripts} -- Folder with helper scripts for generating a~more complex test profiles.
        \end{itemize}
        \item \texttt{logs} -- After the~execution of the~generator, the~logs will be stored here.
        \item \texttt{src} -- Sources of traffirator are stored in this folder and subfolders corresponding to the~Java packages.
    \end{itemize}
\end{itemize}

\chapter{User Documentation of Traffirator}
\label{chapter-appendix-traffirator}

\textbf{Traffirator} is a~traffic generator for the~\textbf{PCRF} server placed within the~LTE network. Traffirator generates traffic for the~\textbf{Gx} and \textbf{Rx} interfaces of the~PCRF server. It simulates user equipments connected to the~LTE network, which might perform calls and usual maintenance of the~user sessions.

The~generator is built on a~concept of test profiles and scenarios. For each execution, there has to be defined a~test profile. Test profiles are sequences of points in time, for each time, the~count of active scenarios, which should be kept up, has to be specified. The~scenario represents a~single user device and it is, in fact, an~automaton. The~nodes of the~automaton can define a~sequence of steps which will be done when the~node is processed. The~steps are only of two types -- receive a~message and send a~message.

Unlike test profiles, scenarios cannot be defined dynamically and are hard-coded in traffirator. In the~definition of the~test profile, we can only specify the~type of the~scenario, which will be used. For the~time being there are only 5 types of scenarios -- \textit{Call Center Employee}, \textit{Call Performance Scenario}, \textit{Classic User}, \textit{Malfunctioning Cell-phone}, \textit{Travelling Manager}.

The~project is released under the~\textbf{MIT} license and uses the~\textbf{jDiameter} library for the~communication over the~Gx and Rx interfaces.

\section{Installation and Compilation}

For the~time being, traffirator is distributed only in the~form of the~GitHub repository. If we want to use traffirator we have to clone the~repository. By the~following command, we clone the~repository to the~current directory.

\myvspace
\begin{lstlisting}[language=sh,frame=single]
> git clone https://github.com/Neloop/traffirator.git
\end{lstlisting}
\myvspace

After downloading the~sources of traffirator we can build the~whole project with \textbf{maven}. Maven has to be executed in the~root directory of the~project. During the~first execution, maven will download all dependencies of the~project, mainly including jDiameter and its dependencies.

\myvspace
\begin{lstlisting}[language=sh,frame=single]
> mvn clean package
\end{lstlisting}
\myvspace

The~previous command will compile Java sources into class files and package the~whole solution into jar file placed in \texttt{target} directory. The~resulting jar file does not contain the~dependencies of the~project, these are placed in the~maven repository folder. To get the~dependencies closer to the~project, we can run following command, which will copy all the~project dependencies in the~\texttt{target/dependency} directory.

\myvspace
\begin{lstlisting}[language=sh,frame=single]
> mvn dependency:copy-dependencies
\end{lstlisting}
\myvspace

Copying the~dependencies is optional and should be used only if the~project jar file will not be executed using maven.

\section{Configuration}

The~traffirator project contains three configuration methods. There is a~configuration of traffirator itself, a~configuration of the~logging framework and a~configuration of the~jDiameter stacks. All three has to be provided upon execution.

\subsection{Traffirator Configuration}

The~configuration of the~generator is defined in the~\textbf{YAML} configuration format. The~YAML file has to be provided upon execution as a~command line argument. The~configuration configures some general attributes of the~execution and also the~test profile, which will be used. The~items of the~configuration are as follows:

\begin{itemize}
    \conciseitemize
    \item \texttt{description} -- Textual description of this configuration and the~test profile defined in this file.
    \item \texttt{threadCount} -- Number of threads, which will be created in the~common executor service.
    \item \texttt{summary} -- Contains path, where the~summary log file should be placed after execution.
    \item \texttt{statistics} -- Defines the~options for statistics outputs from the~traffirator.
    \begin{itemize}[topsep=0pt]
        \conciseitemize
        \item \texttt{logFile} -- The~path, where the~statistics file will be placed.
        \item \texttt{samplingPeriod} -- Sampling period in milliseconds, which will be used as period between the~collections of information from the~generator.
    \end{itemize}
    \item \texttt{profile} -- Definition of the~profile, which will be executed.
    \begin{itemize}[topsep=0pt]
        \conciseitemize
        \item \texttt{burstLimit} -- Burst limit corresponds to the~maximum number of new scenarios that can be created in one second.
        \item \texttt{end} -- Defines the~point in time, when the~execution should end, it is defined in seconds.
        \item \texttt{flow} -- Contains the~list of the~profile entries.
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item \texttt{start} -- The~time, when this profile item begins and should be applied, it is defined in seconds.
            \item \texttt{scenarios} -- Contains the~list of scenarios used in this profile item.
            \begin{itemize}[topsep=0pt]
                \conciseitemize
                \item \texttt{type} -- The~type of scenarios to which the~amount in the~item \texttt{count} is related.
                \item \texttt{count} -- The~number of scenarios with particular type, which will be spawned in the~time defined in \texttt{start} item. The~count is defined in absolute numbers and not incremental numbers.
            \end{itemize}
        \end{itemize}
    \end{itemize}
\end{itemize}

The~example configuration, which can be taken as a~reference, is placed in the~\texttt{examples/example-config.yml} file.

For the~definition of the~scenarios, following types can be used:

\begin{itemize}
    \conciseitemize
    \item \textit{CallCenterEmployee} -- Represents a~user device, which is used by the~call center employee. The~device is making a~lot of calls with a~very small delays between the~calls.
    \item \textit{CallPerformance} -- Scenario used for intense testing of the~PCRF server, all the~delays are small and it is focused on making a~single call and then end.
    \item \textit{ClassicUser} -- Scenario used for real-life testing, it represents classical user of the~telecommunication network, who is making some normal amount of calls and have a~high delays between operations.
    \item \textit{MalfunctioningCellPhone} -- The~device, which is behaving in an~unexpected way, it is frequently disconnecting and connecting and also losing a~signal. The~probability of making a~call is very low.
    \item \textit{TravellingManager} -- The~travelling manager scenario is a~lot like call center employee, manager makes a~lot of calls, but with a~difference of often changes of the~location. This is done by updating the~session information, which are highly probable in this scenario.
\end{itemize}

\subsection{Log4j Configuration}

The~configuration of the~log4j logging framework is defined in the~resources directory of the~project (\texttt{src/main/resources/log4j.properties}) and can be modified before the~execution, to meet the~current needs. The~description of the~configuration items can be found in the~appropriate project documentation of log4j.

\subsection{jDiameter Configuration}

Traffirator contains two Diameter stacks -- Gx and Rx. Both of them are used from the~jDiameter library, which has its own configuration of these stacks. The~configuration for a~stack is in \textbf{XML}, in which the~peer to which the~stack will connect is defined. In addition, there are also some attributes of the~connection, such as the~Diameter application used during the~capabilities exchange. There are also defined some internals, such as timeouts for default Diameter messages or configuration of threading.

Both of the~configuration files must be placed in the~resources directory. The~Gx stack should be configured in the~\texttt{gx-client-config.xml} and the~Rx stack in the~\texttt{rx-client-config.xml} file. Both of the~files should be revised before the~execution to apply correct domain names and IP addresses of the~PCRF server.

The~definition of the~format can be found in the~documentation of jDiameter. It is defined as AsciiDoc in the~documentation directory\footnote{\url{https://github.com/RestComm/jdiameter/tree/master/core/docs/sources-asciidoc/src/main/asciidoc}} within the~official source codes of jDiameter. The~whole documentation is also accessible as a~web-page on the~RestComm webpages\footnote{\url{https://www.restcomm.com/docs/core/diameter/Diameter_User_Guide.html\#_jdiameter_configuration}}.

\section{Execution}

Running traffirator is rather easy, we just have to know the~path to the~configuration file, which we want to use. Maven project in traffirator is setup with the~support of the~maven exec plugin, so the~execution can be started by executing the~following command.

\myvspace
\begin{lstlisting}[language=sh,frame=single]
> mvn exec:java -q 
    -Dexec.args="--config=./examples/example-config.yml"
\end{lstlisting}
\myvspace

The~argument \texttt{-Dexec.args} is used to specify the~command line arguments for the~execution of the~project. There are only two arguments, which are defined in the~traffirator application: \texttt{help} and \texttt{config}. If the~\texttt{help} option is given, traffirator will display its help message with usage and description of all options. The~\texttt{config} option is used to hand over the~path for the~YAML configuration to the~application. The~given configuration file is loaded and defined test profile executed.

\section{Outputs}

For the~time being, traffirator supports only one output file, if we omit the~classical log. The~output is statistics log, which contains some interesting or useful statistics from the~whole execution. The~statistics will be stored in the~path which was given in the~configuration. The~log contains named columns with the~corresponding values in the~rows, columns are separated by the~tabulator character.

The~statistics log has following columns:

\begin{itemize}
    \conciseitemize
    \item \texttt{Time} -- This column contains the~current timestamp when the~row was sampled, in milliseconds.
    \item \texttt{ScenariosCount} -- The~scenarios count column contains the~number of currently active scenarios.
    \item \texttt{TimeoutsCount} -- The~column containing the~number of timeouts since the~last sampled point in time.
    \item \texttt{SentCount} -- Contains the~number of sent messages, which was sent since the~previous sampling.
    \item \texttt{ReceivedCount} -- Relative number of received message since the~last sampling.
    \item \texttt{FailuresCount} -- The~column contains the~number of failures, which was observed since the~last sampling time.
    \item \texttt{ProcessLoad} -- This column contains the~current load of the~traffirator process.
\end{itemize}

\chapter{User Documentation of Testing Framework}
\label{chapter-appendix-framework}

The~testing framework for the~\textbf{PCRF} server developed in the~context of the thesis is a~collection of \textit{shell} scripts, which generally run defined test profiles using \textit{traffirator} against PCRF. During the~testing, the~instrumentation using DiSL takes place, which will mine the~data from MediationZone used as a~PCRF runtime.

The~testing is designed and supposed to be used only with the~PCRF implementation provided by the~telecommunication provider. There are hard-coded paths and some expectations, which prohibit out-of-box usage on a~server with a~different setup.

The~provided testing framework will, for all test profiles, instrument MediationZone, cleanup the~database before running traffirator. Then traffirator is executed with the~definition of some particular test profile. After that, data from traffirator, MediationZone and Couchbase are collected and plotted in form of PDF files with the~use of the~R statistics software.

\section{Dependencies}

Before executing the~testing framework there are few dependencies which have to be satisfied. Operating system under which the~framework runs is supposed to be Linux, preferably \textbf{CentOS}. The~testing framework itself should be placed in the~\texttt{/opt/pcrf-mff} directory, this path is hard-coded in some of the~scripts or configuration and should not be changed.

As stated, the~framework should be executed only on the~server with installed MediationZone, which contains the~PCRF logic. MZ has to be installed in the~\texttt{/opt/CZaPCRF/mz} directory and has to contain following workflows -- \textit{CZ\_PCV.WFL\_GxRx}, \textit{CZ\_PCV.WFL\_Log} and \textit{CZ\_PCV.WFL\_Dump}.

Another dependency is \textit{Couchbase} NoSQL database. The~Couchbase has to be installed in the~\texttt{/opt/couchbase} directory and it must run as a~server on a~localhost. All other configuration of Couchbase is handled by the~testing framework or MediationZone.

Because we use \textit{traffirator} for the~traffic generation we have to have it installed. Traffirator has to be placed in the~\texttt{/opt/traffirator} directory alongside the~testing framework. Again, some paths are hard-coded and it is expected that traffirator is on the~same directory level as the~root of the~framework.

For the~successful build and execution of traffirator and DiSL instrumentation, we need to install \textit{maven} build system, and \textit{Java JDK}. Maven, Java compiler and Java runtime have to be present on the~system path.

For plotting the~data into sensible plots within PDF files, the~\textit{R} statistical software is used. R and Rscript have to be present on the~system path in order to plot the~graphs successfully. In addition to the~base R application, there has to be installed following plugins -- \textit{foreach}, \textit{ggplot2} and \textit{jsonlite}.

The~DiSL instrumentation framework can be installed and built by the~provided script in the~file \texttt{pcrf-mff/disl/prepare.sh}. This script will download two versions of the~DiSL, the~stable 2.1 version and the~latest version from the~source subversion repository. After downloading the~sources, both DiSL versions are built, this includes both parts, the~part written in Java and the~agent part written in C. DiSL resides in the~\texttt{/opt/disl} directory and there are two subdirectories, \texttt{disl-2.1} for the~stable version and \texttt{trunk} for the~latest version.

\section{Execution}

The~testing framework has predefined test profiles which are executed in a~\textit{loop}. Before each execution of the~test profile, the~server is properly setup and after the~execution, the~cleanup and analysis of the~results are performed. There are two main components, the~traffic generator, and the~PCRF server, both of them can be run with specific \textit{CPU affinity}. The~setup of the~affinity is used for the~load distribution if the~underlying server contains two or more processors. Both components can then operate on the~different processors and do not affect each other.

The~framework should be run with the~\texttt{pcrf-mff/real-scenarios/run.sh} script, there are three mandatory positional parameters -- taskset mask for traffirator, taskset mask for the~PCRF and RAM quota for the~Couchbase bucket. Follows the~description of the~parameters:

\begin{itemize}
    \conciseitemize
    \item \textit{Taskset mask for traffirator} -- The~processor affinity mask which will be handed over to the~taskset command with \texttt{-c} option enabled upon execution of traffirator.
    \item \textit{Taskset mask for PCRF} -- The~affinity of the~processor which will be applied to the~restarted ECSA within MediationZone. The~taskset command is executed with \texttt{-c} option enabled.
    \item \textit{RAM quota for the~Couchbase bucket} -- The~default amount of RAM which is assigned to the~newly created bucket in Couchbase is 100MB, which is very low and not sufficient for the~testing. Therefore, the~default value can be redefined by this parameter, which is also in megabytes. The~actual value should be influenced by the~amount of available RAM on the~server and expected number of data stored in the~database.
\end{itemize}

The~script will execute the~measurement for all defined test profiles, for the~time being, there are only 4 test profiles. Every test profile has a~duration of \textit{2.4 hours}, in addition, there are 10 minutes pauses between executions. The~total execution time of the~testing suite is roughly \textbf{10 hours}.

The~results will be accessible in the~\texttt{pcrf-mff/plots/out/\textless timestamp\textgreater} directory, where timestamp was captured on the~directory creation. This directory will contain all copied logs from traffirator, instrumentation, and statistics from Couchbase. In addition, there should be PDF files with graphs plotted with the R statistical software. Graphs contain some useful and suitable metrics.

Note that the~execution will overwrite the~configuration of the~execution contexts from MediationZone. It is strongly advised to \textbf{backup} the~original configuration placed in \texttt{/opt/CZaPCRF/mz/etc/executioncontext.xml} to a~different directory as the~execution might fail and the~original configuration will not be copied back.

\chapter{Description of PCRF Flows}
\label{chapter-appendix-messages}

This appendix contains a~description of basic PCRF message content in the~context of different states and flows. In the~following tables, mandatory AVPs are bold, values are examples and may not represent real values. Where applicable, a~description of the~value is provided.

Because the~content of messages, especially AVPs, can change drastically based on user, device or type of call, introducing all variants of messages is not the~goal of this appendix. Only example values for particular messages will be listed. The~following tables are summarized excerpts from the~descriptions of particular messages in the~ABNF form and from the~descriptions of the~AVPs. These descriptions can be found in the~appropriate specifications.

\section{Connect}
\subsection*{CCR}
\input{table/appendix-a-connect-1.tex}
\subsection*{CCA}
\input{table/appendix-a-connect-2.tex}

\section{Disconnect}
\subsection*{CCR}
\input{table/appendix-a-disconnect-1.tex}
\subsection*{CCA}
\input{table/appendix-a-disconnect-2.tex}

\section{Update}
\subsection*{CCR}
\input{table/appendix-a-update-1.tex}
\subsection*{CCA}
\input{table/appendix-a-update-2.tex}

\section{Lost Connection}
\subsection*{CCR}
\input{table/appendix-a-lost-connection-1.tex}
\subsection*{CCA}
\input{table/appendix-a-lost-connection-2.tex}

\section{Call}
\subsection{Call Init}
\subsubsection*{AAR}
\input{table/appendix-a-call-init-1.tex}
\subsubsection*{RAR}
\input{table/appendix-a-call-init-2.tex}
\subsubsection*{AAA}
\input{table/appendix-a-call-init-3.tex}
\subsubsection*{RAA}
\input{table/appendix-a-call-init-4.tex}
\subsubsection*{AAR}
\input{table/appendix-a-call-init-5.tex}
\subsubsection*{AAA}
\input{table/appendix-a-call-init-6.tex}

\subsection{Call Update Codec}
\subsubsection*{AAR}
\input{table/appendix-a-call-update-1.tex}
\subsubsection*{RAR}
\input{table/appendix-a-call-update-2.tex}
\subsubsection*{AAA}
\input{table/appendix-a-call-update-3.tex}
\subsubsection*{RAA}
\input{table/appendix-a-call-update-4.tex}

\subsection{Call Termination}
\subsubsection*{STR}
\input{table/appendix-a-call-termination-1.tex}
\subsubsection*{RAR}
\input{table/appendix-a-call-termination-2.tex}
\subsubsection*{STA}
\input{table/appendix-a-call-termination-3.tex}
\subsubsection*{RAA}
\input{table/appendix-a-call-termination-4.tex}

\subsection{Call Lost Connection}
\subsubsection*{CCR}
\input{table/appendix-a-call-lost-1.tex}
\subsubsection*{ASR}
\input{table/appendix-a-call-lost-2.tex}
\subsubsection*{CCA}
\input{table/appendix-a-call-lost-3.tex}
\subsubsection*{ASA}
\input{table/appendix-a-call-lost-4.tex}
\subsubsection*{STR}
\input{table/appendix-a-call-lost-5.tex}
\subsubsection*{RAR}
\input{table/appendix-a-call-lost-6.tex}
\subsubsection*{STA}
\input{table/appendix-a-call-lost-7.tex}
\subsubsection*{RAA}
\input{table/appendix-a-call-lost-8.tex}
