\chapter{Analysis}
\label{chapter-analysis}

In this chapter, we will perform an~analysis of the~traffic generation and also discuss the~options for data collection from the~PCRF server. For the~traffic generation, we need to decide how and what workload will be generated. The~regression tests from the~provider can work as a~base for this decision, there were also provided base workflows which are processed by the~PCRF server.

Based on the~provided base workflows, we have to identify flows representing states or particular actions. For example, during a~connection of a~device, there is a~flow of messages which consists of CCR and CCA messages, the~request is sent by PCEF and answer by PCRF. These flows were identified and are described in the~following sections.

When we have the~communication flows identified, we need to decide how these flows will be connected. For example, we can make two calls right after each other in a~combined flow. The~basic connection of the~flows is by making a~sequence. This approach is used by the~seagull traffic generator but does not conform to the~real-life use, because it is not flexible enough and does not simulate a~real user device.

Other option to connect the~states is by forming a~graph with directed connections. Considering this, there might be nodes with multiple children, therefore, we need to be able to decide what will be the~next processed node. This leads us to the~\textbf{automatons} with custom defined transition function. For the~thesis needs, the~transition function can be defined in form of probabilities of transitions between the~nodes. In this form, the~automaton is defining a~particular usage of a~phone in a~telecommunication network.

Based on the~transition function, we can simulate distinct types of users with distinct automatons. For example, we can make users which are calling a~lot, or classical user of the~network with a~reasonable flow of the~calls and other actions. There can also be a~special scenario which is designed only to test the~performance of the~server, etc.

With defined automatons, we need to have a~way how to execute them asynchronously and also a~way to define when and what automatons will be active. This definition shall be called \textbf{test profile} and will be used by the~traffic generator to spawn new automatons or destroy currently active automatons.

Test profiles which will be defined in the~thesis should reflect the~need for real-life testing, therefore, there should definitely be a~profile which simulates a~day in the~telecommunication network. Other interesting cases might be a~performance testing of the~PCRF server or stress testing.

The~\textbf{metrics} which should be collected from the~PCRF server are essential for the~performance evaluation performed in the~thesis. The~analysis of the~metrics should be done in consideration of real-life performance evaluation. Therefore, useful metrics like latency of the~messages or number of the~processed messages have to be collected. The~analysis also has to include approaches which might be used for data collection and what are the~specifics of different approaches.

\section{Traffic Generation Analysis}

The~base for the~traffic generation design is an~analysis of how the~data should be generated and what are the~flows of the~messages used within the~PCRF communication. Basic flows of the~messages are defined by the~3GPP~TS~29.213~\cite{3gpp.29.213} specification, specialized flows for the~\textit{Gx} interface can be found in 3GPP~TS~29.212~\cite{3gpp.29.212} and for the~\textit{Rx} interface in 3GPP~TS~29.214~\cite{3gpp.29.214}.

On top of the~specifications, we can specify how the~basic flows will be connected to represent a~usual communication of one user device. The~representation of a~user device and connections between the~basic flows shall be called a~\textbf{scenario}. Additionally, a~scenario can specify some parameters of the~flows or their connections.

Assuming that each scenario represents one specialized user type, the~implementation of the~traffic generator can contain multiple templates of scenarios which can be instantiated and executed. The~definition of the~scenarios flow and their numbers shall be called a~\textbf{test profile}. A~test profile is a~definition of what scenarios should be active at what time during a~test. Based on this, the~generator should create or destroy scenarios at times specified in the~test profile provided at the~beginning of the~test execution.

\subsection{PCRF Communication Flows}
\label{subsection-pcrf-flows}

From a~high-level point of view, there are only four major states that appear in the~communication flow of a~single user device. These are denoted as \textit{Connect}, \textit{Update}, \textit{Disconnect}, \textit{Lost Connection} and \textit{Call}. A~basic depiction of the~states and proposed connections between them can be found in Figure~\ref{figure-scenario-base-automaton}. Except for the~Call state, the~states are simple request-answer message flows. The~Call can be a~bit more complex and as such, it can be viewed as an~independent unit with nested states inside.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-base-automaton.pdf}
\caption{Scenario Base Automaton}
\label{figure-scenario-base-automaton}
\end{figure}

The~call state can have multiple types based on the~call attributes, the~basic distinction is between a~\textit{Regular Call} and a~\textit{Conference Call}. The~Conference Call connects multiple users in one conversation and the~message flow is quite complex. In real life, conference calls are very rare and therefore out of the~scope of this thesis. The~Regular Call state contains four sub-states: \textit{Call Init}, \textit{Call Update Codec}, \textit{Call Termination}, and \textit{Lost Connection}. These sub-states with their proposed connections are shown in Figure~\ref{figure-call-automaton}.

\begin{figure}[!htb]
\centering
\includegraphics{./img/drawio/call-automaton.pdf}
\caption{Call Automaton}
\label{figure-call-automaton}
\end{figure}

All states define message flows and can contain an~arbitrary number of steps which are executed when entering the~state. Steps can be represented as requests or answers, both can be sent from or to the~Gx or Rx interfaces. The~description of the~particular states with their flows depicted using UML sequence diagrams follows (a detailed description of the~messages and their AVPs with values can be found in Appendix~\ref{chapter-appendix-messages}).

\subsubsection{Connect}

The~Connect action is performed when the~device connects to the~LTE network, after it gets an~IP address from the~PGW. The~initial connection comes from the~Gx interface and is established to allocate the~default network bearer and to let the~PCRF know about the~newly connected device with the~given session identification.

During connection initiation, requests may contain further parameters which the~bearer or session will have. For example, there may be a~list of predefined events that can arrive in update messages from the~Gx interface. No other events should be accepted by the~PCRF.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-state-connect.pdf}
\caption{Connect Scenario State}
\label{figure-state-connect}
\end{figure}

\noindent
Description of the~flow from Figure~\ref{figure-state-connect}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{CCR-I} initial request containing information about newly connected device
    \item \textit{CCA-I} answer to the~previous message
\end{enumerate}

\subsubsection{Disconnect}

The~Disconnect action is initiated by the~user device itself and redirected by the~PCEF to the~PCRF, which is instructed only to destroy the~Gx session without any further actions needed.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-state-disconnect.pdf}
\caption{Disconnect Scenario State}
\label{figure-state-disconnect}
\end{figure}

\noindent
Description of the~flow from Figure~\ref{figure-state-disconnect}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{CCR-T} termination request which is performed by the~device when it is going to be turned off
    \item \textit{CCA-T} answer to the~previous message
\end{enumerate}

\subsubsection{Update}

The~Update action is performed if there was a~change in the~session information, it can arrive only from the~Gx interface. The~events which can arrive in an~update have to be previously registered during the~connection of the~device. This can be used for example if there is a~revalidation request for the~session or an~update of the~device location. The~update is the~only state that can be entered at any time after the~user device was connected to the~LTE network.

The~example of the~Update action is revalidation request for the~session, because the~session needs to be periodically renewed. As such, the~Update action should ideally have its own automaton or any other kind of processing unit, which should be triggered periodically and asynchronously on any other kinds of messages. But for the~simplification of the~scenario automatons, we consider the~Update action as a~part of the~combined automatons.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-state-update.pdf}
\caption{Update Scenario State}
\label{figure-state-update}
\end{figure}

\noindent
Description of the~flow from Figure~\ref{figure-state-update}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{CCR-U} update request when event, which includes the~user device, occurs in the~network
    \item \textit{CCA-U} answer to the~previous message
\end{enumerate}

\subsubsection{Lost Connection}

Lost Connection is a~special type of the~Update state. It is considered separately because of the~different internal logic and because the~steps that follow after the~Lost Connection state are different from those in the~normal update of a~session. If the~device is considered disconnected due to lost connection, the~network will hand over this information to the~PCRF over the~Gx interface. After receiving the~message, the~PCRF can destroy the~Gx session like in the~normal disconnect.

\myvspace
\noindent
Description of the~flow from Figure~\ref{figure-state-update}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{CCR-U} update request which notifies PCRF that the~user device stopped responding
    \item \textit{CCA-U} answer to the~previous message
\end{enumerate}

\subsubsection{Call}

If the~user is requesting call, the~device is already connected to the~network and the~Gx session is initialized. The~call is divided into four sub-states:

\begin{itemize}
    \conciseitemize
    \item Call Init
    \item Call Update
    \item Call Termination
    \item Call Lost Connection
\end{itemize}

\paragraph{Call Init}

The~initiation of the~call originates from the~Rx interface, the~CSCF requests a~bearer with a~predefined bandwidth and codec from the~PCRF. On top of that, a~new Rx call session is created. When the~bearer allocation is acknowledged by the~PCRF, the~provisioning and charging rules are sent to the~PCEF, where they should be enforced.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-state-call-init.pdf}
\caption{Call Init Scenario State}
\label{figure-state-call-init}
\end{figure}

\noindent
Description of the~flow from Figure~\ref{figure-state-call-init}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{AAR} request indicates start of the~a call and the~need of individual bearer for voice data, the~request comes with the~information about used codec and bandwidth
    \item \textit{RAR} sends provisioning rules installation request to the~PCEF, AAA is successful answer to the~previous Rx request
    \item \textit{RAA} answer to the~rules installation request
    \item \textit{AAR} can be noop or some initial change of the~call attributes, like enabling uplink and downlink voice channels
    \item \textit{AAA} answer to the~previous request
\end{enumerate}

\paragraph{Call Update Codec}

Just like call initiation, call update also comes from the~Rx interface and usually happens if a~change of the~codec is requested or if different bandwidth should be used. The~request for change may or may not trigger the~installation of rules to the~PCEF. For simplicity, we assume a~rule change is always propagated to the~PCEF.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-state-call-update.pdf}
\caption{Call Update Codec Scenario State}
\label{figure-state-call-update}
\end{figure}

\myvspace
\noindent
Description of the~flow from Figure~\ref{figure-state-call-update}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{AAR} request indicates change of the~codec during the~call
    \item \textit{RAR} sends provisioning rules installation request to the~PCEF, AAA is successful answer to the~previous Rx request
    \item \textit{RAA} is an~answer to the~rules installation request
\end{enumerate}

\paragraph{Call Termination}

When a~user decides to end a~call, the~CSCF will send the~session termination request which destroys the~established Rx call session and let the~PCEF know that the~established bearer can be destroyed.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-state-call-termination.pdf}
\caption{Call Termination Scenario State}
\label{figure-state-call-termination}
\end{figure}

\noindent
Description of the~flow from Figure~\ref{figure-state-call-termination}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{STR} the~device requested end of the~call which is delivered to the~PCRF in form of this message
    \item \textit{RAR} requests bearer destruction by sending message to the~PCEF, STA is successful answer to the~previous termination request
    \item \textit{RAA} answer to the~bearer destruction
\end{enumerate}

\paragraph{Call Lost Connection}

Lost Connection during a~call is a~bit different from general Lost Connection. The~initial steps are the~same -- the~PCEF signals to the~PCRF that the~user device stopped responding and therefore requests a~destruction of the~session. This destroys only the~session attached to the~Gx interface, if we are in the~middle of the~call there is also the~call session which must be destroyed by sending a~proper request to the~Rx interface. At the~end of the~exchange there is a~(strictly unnecessary) no-op session termination request originating on the~CSCF with appropriate request for termination of bearer directing to the~PCEF.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-state-call-lost.pdf}
\caption{Call Lost Connection Scenario State}
\label{figure-state-call-lost}
\end{figure}

\myvspace
\noindent
Description of the~flow from Figure~\ref{figure-state-call-lost}:

\begin{enumerate}[topsep=0pt]
    \conciseitemize
    \item \textit{CCR-U} update request which is the~same as for general lost connection state
    \item \textit{ASR} sending new request to destroy ongoing call, CCA-U answer to the~previous message
    \item \textit{ASA} answer to the~previous request
    \item \textit{STR} is not necessary and on the~PCRF it is a~noop
    \item \textit{RAR} requests bearer destruction by sending message to the~PCEF, STA is successful answer to the~previous termination request
    \item \textit{RAA} answer to the~bearer destruction
\end{enumerate}

\subsection{Scenarios Description}

There are two principal approaches to designing scenarios and connecting the~basic communication states. The~first one is simple sequencing the~states right after each other. This approach is static, with every scenario having predefined steps and length. The~\textit{seagull} traffic generator uses this approach to define scenarios. For more realistic testing, scenarios must be dynamic and simulate user devices with varying transitions between states, including loops and conditions. To meet this requirement, we create scenario automatons with stochastic transitions between states.

If we combine together the~basic automata from the~previous subsection, we get a~\textbf{mixin automaton} that represents a~single user device on its everyday journey, to be used to generate a~pseudo real-life flow. The~mixin automaton, designed for the~purposes of testing the~PCRF, can be found on Figure~\ref{figure-scenario}. One of the~interesting attributes of the~automaton, which will affect its behavior, are the~\textit{probabilities of transitions} between the~automaton nodes. Using the~probabilities, we can define a~flow which simulates, for example, an~employee in a~call center who is making many calls per day (the~probability of the~transition between ending a~call and starting another one will be high).

\begin{figure}[htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario.pdf}
\caption{Scenario Automaton}
\label{figure-scenario}
\end{figure}

Other important attributes of the~scenarios are \textit{delays} and \textit{timeouts}. A~delay is the~time spent waiting for the~next action. In scenarios we can have delays between scenario nodes or delays between particular steps of the~state, this simulates either waiting or the~duration of the~call, etc. The~delays are variable, either drawn from an~interval or defined as a~single value with relative variability. Timeouts, on the~other hand, are static values which define the~duration used when waiting for an~answer. If a~timeout expires without receiving an~answer, the~scenario is declared failed.

In a~discussion with the~telco provider, we have specified following scenarios of interest from the~PCRF point of view. All scenarios should reflect real behavior of the~users and their devices. A~detailed description of the~scenarios, with probabilities of transitions, delays between nodes and timeouts, follows.

\begin{itemize}[topsep=0pt]
    \conciseitemize
    \item Call Center Employee
    \item Malfunctioning Cell-Phone
    \item Travelling Manager
    \item Classic User
    \item Call Performance Scenario
\end{itemize}

\subsubsection{Call Center Employee}

This scenario models a~call center employee who is making multiple calls in a~row with little or no delays. After a~successful connection to the~network, the~cell phone has a~high chance to initiate a~call. The~calls in this scenario are of medium length, expressed in several minutes. During the~call, the~chances of update of the~codec are low. The~aim of the~scenario is to make many calls within short time and nothing else. The~resulting automaton is depicted in Figure~\ref{figure-scenario-call-center}. The~variability for all delays is 20\% and the~timeouts for all answers are set to 3 seconds.

\subsubsection{Malfunctioning Cell-Phone}

The~scenario simulates a~malfunctioning cell phone in the~provider network. A~malfunctioning cell phone behaves incorrectly, it has very high probability of disconnection. Calls are rare and if they do happen, they are stopped due to a~lost connection. On top of that, the~delays are very small and all actions are done almost instantly. The~resulting automaton is depicted in Figure~\ref{figure-scenario-malfunctioning}. The~variability for all delays is 20\% and the~timeouts for all answers are set to 3 seconds.

\subsubsection{Travelling Manager}

The~scenario is centered on users of the~network who are calling a~lot and often changing locations. In the~real-life, this scenario can be applied to managers, who are travelling to distinct locations and in the~process they are making calls. To model this, the~updates have a~high probability and the~same goes for calls.

Unique to this scenario is a~special state in the~call which performs an~update of the~user location. The~probability of processing this state is quite high during the~call and the~update can be done repeatedly. The~resulting automaton is depicted in Figure~\ref{figure-scenario-travelling}. The~variability for all delays is 20\% and the~timeouts for all answers are set to 3 seconds.

\clearpage

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-call-center.pdf}
\caption{Call Center Scenario Automaton}
\label{figure-scenario-call-center}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-malfunctioning.pdf}
\caption{Malfunctioning Cell-Phone Scenario Automaton}
\label{figure-scenario-malfunctioning}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-travelling.pdf}
\caption{Travelling Manager Scenario Automaton}
\label{figure-scenario-travelling}
\end{figure}

\clearpage

\subsubsection{Classic User}

Classic User is a~scenario which should represent a~normal everyday user of the~network. After successful connection, there is a~high chance of starting the~call, but with significant delay. During the~call, updates of the~codecs are rare and after the~call ends, there is a~high chance of starting another call, again with a~significant delay. The~resulting automaton is depicted on Figure~\ref{figure-scenario-classic}. The~variability for all delays is 20\% and the~timeouts for all answers are set to 3 seconds.

\subsubsection{Call Performance Scenario}

Rather than approximating a~real use case, this scenario is designed for performance and stress testing. After the~initial connection, the~scenario starts a~call with high probability. Codec update during the~call is quite likely, but after the~first update, the~call is likely to end. When the~call finishes, the~highest probability is to disconnect the~device from the~network. In brief, this scenario is supposed only to connect, make one call and disconnect, making it easy to relate the~scenario execution frequency with the~call throughput.

Almost all delays in the~scenario are set to 10 seconds, which should ensure a~quick rotation of the~states. Due to the~low delays, the~performance in this scenario is not directly comparable to other scenarios. The~resulting automaton is depicted in Figure~\ref{figure-scenario-call-perf}. The~variability for all delays is 20\% and the~timeouts for all answers are set to 3 seconds.

\clearpage

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-classic.pdf}
\caption{Classic User Scenario Automaton}
\label{figure-scenario-classic}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/drawio/scenario-call-perf.pdf}
\caption{Call Performance Scenario Automaton}
\label{figure-scenario-call-perf}
\end{figure}

\clearpage

\subsection{Test Profiles}
\label{subsection-analysis-test-profiles}

The~\textbf{test profiles} are predefined sequences of scenarios, which should be started at given experiment time. In more detail, a~profile consists of profile entries, one profile entry is uniquely identified by its start time and contains the~list of scenarios (with their counts) which should be active from that time on. If the~profile entry requests a~higher scenario count than in the~previous entry, new scenarios should be spawned. Similarly, if the~profile entry requests a~lower scenario count, some currently active scenarios should be destroyed to reflect the~prescribed scenario count. Based on this, custom profiles can be defined to reflect for example a~normal day in the~telecommunication network. The~following list contains the~profiles defined for the~purposes of the~PCRF testing:

\begin{itemize}
    \conciseitemize
    \item Call performance testing
    \item Stress testing
    \item Basic real-life testing
    \item Big event or catastrophe
\end{itemize}

The~description of the~listed profiles follows. The~profiles were created and consulted together with the~representatives from the~telecommunication provider. Some of the~profiles should reflect real numbers (or slightly higher) of the~user devices that can be seen in everyday traffic.

\subsubsection{Call Performance Testing}

A~profile designed to test the~performance of the~PCRF server and determine the~maximum number of messages which the~server is able to process. The~flow of the~profile is as follows -- every 20 minutes the~number of active scenarios increases by 800. The~total duration of the~test is 24 hours and the~profile uses only the~Call Performance scenario. The~number of scenarios at the~end of the~test is 57600. The~intensity of the~profile is shown on Figure~\ref{figure-profile-call-perf}.

\subsubsection{Stress Testing}

A~profile designed to test the~stability of the~server and its ability to compensate and recover from a~massive number of connected user devices. At the~beginning, there are 500000 active scenarios which are executed for 100 minutes, after this time the~count is decreased to 1000 active scenarios. This action is repeated three times, each after 8 hours. The~profile uses only the~Call Performance scenario to simulate users and the~duration is 24 hours. The~intensity of the~profile is depicted in Figure~\ref{figure-profile-stress}.

\clearpage

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/profile-call-perf.pdf}
\caption{Call Performance Test Profile}
\label{figure-profile-call-perf}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/profile-stress.pdf}
\caption{Stress Test Profile}
\label{figure-profile-stress}
\end{figure}

\clearpage

\subsubsection{Real-life Testing}

The~Real-life Testing profile is simulated through the~whole day (24 hours) and should simulate a~real-life usage profile of the~telecommunication provider network. The~described profile contains real numbers provided by the~telecommunication provider. At the~peak of the~day, there are around 1.1 million users and the~number of active users is never lower than 50 thousand. The~flow of the~profile is depicted in Figure~\ref{figure-profile-real}. Table~\ref{table-profile-real} contains the~exact numbers of scenarios at given times.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/profile-real.pdf}
\caption{Real-life Test Profile}
\label{figure-profile-real}
\end{figure}

\begin{table}[!htb]
\centering
\footnotesize
\begin{tabular}{|p{15mm}|p{25mm}|p{25mm}|p{27mm}|p{25mm}|}
\hline
\textbf{Time} & \textbf{Call Center} & \textbf{Classic} & \textbf{Malfunctioning} & \textbf{Travelling} \\
\hline
00:00 & 0 & 50000 & 30 & 0 \\
00:30 & 0 & 30000 & 30 & 0 \\
01:00 & 0 & 20000 & 30 & 0 \\
01:30 & 0 & 15000 & 20 & 0 \\
02:00 & 0 & 15000 & 20 & 0 \\
02:30 & 0 & 10000 & 10 & 0 \\
03:00 & 0 & 10000 & 10 & 0 \\
03:30 & 0 & 8000 & 10 & 0 \\
04:00 & 0 & 10000 & 10 & 0 \\
04:30 & 0 & 13000 & 10 & 0 \\
05:00 & 0 & 20000 & 30 & 0 \\
05:30 & 0 & 35000 & 50 & 0 \\
06:00 & 0 & 60000 & 70 & 10 \\
06:30 & 0 & 100000 & 100 & 30 \\
07:00 & 0 & 200000 & 150 & 50 \\
07:30 & 200 & 300000 & 200 & 100 \\
08:00 & 1000 & 500000 & 300 & 300 \\
08:30 & 2000 & 700000 & 400 & 500 \\
09:00 & 5000 & 900000 & 500 & 1000 \\
09:30 & 5000 & 1000000 & 600 & 2000 \\
10:00 & 5000 & 1100000 & 700 & 3000 \\
10:30 & 5000 & 1100000 & 800 & 5000 \\
11:00 & 5000 & 1150000 & 800 & 5000 \\
11:30 & 5000 & 1050000 & 800 & 5000 \\
12:00 & 3000 & 1000000 & 800 & 5000 \\
12:30 & 2000 & 1000000 & 800 & 4000 \\
13:00 & 6000 & 1000000 & 800 & 5000 \\
13:30 & 7000 & 1000000 & 800 & 6000 \\
14:00 & 8000 & 1000000 & 800 & 6000 \\
14:30 & 8000 & 1000000 & 800 & 7000 \\
15:00 & 8000 & 1000000 & 700 & 6000 \\
15:30 & 7000 & 950000 & 600 & 5000 \\
16:00 & 5000 & 950000 & 500 & 5000 \\
16:30 & 3000 & 900000 & 500 & 3000 \\
17:00 & 1000 & 800000 & 500 & 2000 \\
17:30 & 500 & 750000 & 500 & 1000 \\
18:00 & 0 & 700000 & 300 & 500 \\
18:30 & 0 & 650000 & 300 & 300 \\
19:00 & 0 & 600000 & 200 & 200 \\
19:30 & 0 & 550000 & 200 & 100 \\
20:00 & 0 & 500000 & 200 & 100 \\
20:30 & 0 & 400000 & 200 & 100 \\
21:00 & 0 & 300000 & 100 & 0 \\
21:30 & 0 & 250000 & 100 & 0 \\
22:00 & 0 & 150000 & 50 & 0 \\
22:30 & 0 & 100000 & 50 & 0 \\
23:00 & 0 & 80000 & 50 & 0 \\
23:30 & 0 & 50000 & 30 & 0 \\
\hline
\end{tabular}
\caption{Real-life Test Profile Definition}
\label{table-profile-real}
\end{table}

\subsubsection{Big Event or Catastrophe}

The~base profile for Big event or Catastrophe is the~Real-life Testing profile introduced in the~previous section. In addition to real-life usage activity, there are two events in the~network that lead to the~appearance of active devices. The~first event happens at 10 AM and lasts for an~hour, the~second event is scheduled for 6 PM and lasts for two hours. Both events are identified by an~increased number of active devices, which is continually decreasing during the~event. The~intensity of the~profile is depicted in Figure~\ref{figure-profile-big}. Table~\ref{table-profile-big} contains the~exact numbers of scenarios at given times.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/profile-big-event.pdf}
\caption{Big Event or Catastrophe Test Profile}
\label{figure-profile-big}
\end{figure}

\begin{table}[!htb]
\centering
\footnotesize
\begin{tabular}{|p{15mm}|p{25mm}|p{25mm}|p{27mm}|p{25mm}|}
\hline
\textbf{Time} & \textbf{Call Center} & \textbf{Classic} & \textbf{Malfunctioning} & \textbf{Travelling} \\
\hline
00:00 & 0 & 50000 & 30 & 0 \\
00:30 & 0 & 30000 & 30 & 0 \\
01:00 & 0 & 20000 & 30 & 0 \\
01:30 & 0 & 15000 & 20 & 0 \\
02:00 & 0 & 15000 & 20 & 0 \\
02:30 & 0 & 10000 & 10 & 0 \\
03:00 & 0 & 10000 & 10 & 0 \\
03:30 & 0 & 8000 & 10 & 0 \\
04:00 & 0 & 10000 & 10 & 0 \\
04:30 & 0 & 13000 & 10 & 0 \\
05:00 & 0 & 20000 & 30 & 0 \\
05:30 & 0 & 35000 & 50 & 0 \\
06:00 & 0 & 60000 & 70 & 10 \\
06:30 & 0 & 100000 & 100 & 30 \\
07:00 & 0 & 200000 & 150 & 50 \\
07:30 & 200 & 300000 & 200 & 100 \\
08:00 & 1000 & 500000 & 300 & 300 \\
08:30 & 2000 & 700000 & 400 & 500 \\
09:00 & 5000 & 900000 & 500 & 1000 \\
09:30 & 5000 & 1000000 & 600 & 2000 \\
10:00 & 5000 & 1500000 & 700 & 3000 \\
10:30 & 5000 & 1300000 & 800 & 5000 \\
11:00 & 5000 & 1150000 & 800 & 5000 \\
11:30 & 5000 & 1050000 & 800 & 5000 \\
12:00 & 3000 & 1000000 & 800 & 5000 \\
12:30 & 2000 & 1000000 & 800 & 4000 \\
13:00 & 6000 & 1000000 & 800 & 5000 \\
13:30 & 7000 & 1000000 & 800 & 6000 \\
14:00 & 8000 & 1000000 & 800 & 6000 \\
14:30 & 8000 & 1000000 & 800 & 7000 \\
15:00 & 8000 & 1000000 & 700 & 6000 \\
15:30 & 7000 & 950000 & 600 & 5000 \\
16:00 & 5000 & 950000 & 500 & 5000 \\
16:30 & 3000 & 900000 & 500 & 3000 \\
17:00 & 1000 & 800000 & 500 & 2000 \\
17:30 & 500 & 750000 & 500 & 1000 \\
18:00 & 0 & 1200000 & 300 & 500 \\
18:30 & 0 & 1100000 & 300 & 300 \\
19:00 & 0 & 1000000 & 200 & 200 \\
19:30 & 0 & 800000 & 200 & 100 \\
20:00 & 0 & 500000 & 200 & 100 \\
20:30 & 0 & 400000 & 200 & 100 \\
21:00 & 0 & 300000 & 100 & 0 \\
21:30 & 0 & 250000 & 100 & 0 \\
22:00 & 0 & 150000 & 50 & 0 \\
22:30 & 0 & 100000 & 50 & 0 \\
23:00 & 0 & 80000 & 50 & 0 \\
23:30 & 0 & 50000 & 30 & 0 \\
\hline
\end{tabular}
\caption{Big Event Test Profile Definition}
\label{table-profile-big}
\end{table}

\section{PCRF Data Collection}

As described in Figure~\ref{figure-pcrf-wfl}, the~workflow which implements the~PCRF function has one main processing box called \textbf{Process}. All other boxes are used either for logging or as network stacks connecting \textit{PCRF} with \textit{PCEF} and \textit{CSCF}. Considering this, the~main data collection target should be the~Process box and the~network stacks.

The~Process box contains the~whole PCRF logic defined in \textit{APL}. On execution, this box is transpiled into a~standalone jar file containing a~single class with a~single method representing the~APL code. MediationZone has a~component named \textit{CodeServer} which is responsible for distributing the~jar files on the~workers. All jar files used during execution can therefore be found in the~CodeServer cache directory.

The~Diameter network stacks and the~corresponding boxes are predefined by MediationZone and are also distributed by CodeServer. The~analysis of the~internal implementation of the~MZ and network stacks was done by decompiling these jar files. The~following section contains the~results from performed bytecode analysis.

\subsection{Collected Output}

There are two types of information which can be collected from PCRF. One is the~information from the~Process method describing each processed message, and one is the~information from the~network stacks, which contain mainly network traffic related metrics.

The~data describing the~processed messages contains various bits of information. Every message is uniquely identified by the~\textit{SessionId} and \textit{HopByHop} Identifier. The~message can be of two types, request or answer, plus there are various command types. Another, more interesting information concerns the~time of reception, when message processing started and finished. There is also the~information about answers, whether and when they are sent during request processing. Last but not least, for every message we can monitor the~time spent in the~database actions.

The~information from the~network stack usually concerns accumulative counts or current status numbers. The~Diameter network stack within MediationZone also collects useful statistics. We can obtain the~basic metrics such as the~counts of \textit{incoming} and \textit{outgoing} messages, there are also counts of messages rejected by the~stack and messages to which the~stack replied with the~\textit{TooBusy} answer. From the~Process box, we can get the~counts of messages which are currently being processed, which are waiting for processing, and which were already processed.

To determine the~sizes of the~network queues in the~operating system, we can access the~network interface information. Again for both stacks we can get the~information about the~number of bytes in the~receive and send queues. On Linux, this information can be obtained either with a~system call or with a~specialized application.

\subsection{Measurement Options}

In order to get the~data on the~performance of the~PCRF server, we need a~measurement framework able to collect data from the~PCRF server on the~go. The~measurement framework should be automatically pluggable during the~execution with no or little work. Manual interaction of the~person who is executing the~measurement should not be needed and the~whole process of collecting the~data should be automatic.

The~measurement and collection of data from the~PCRF have to account for the~presence of the~MediationZone, which is running the~PCRF. As described in Section~\ref{section-pcrf-implementation}, MediationZone is an~agent system framework written in the~Java programming language. There is a~concept of workflows which define functionality by connecting processing boxes. Processing boxes contain the~business logic which is defined in a~Java-like DSL and transpiled into Java bytecode.

Because the~PCRF does not offer any monitoring infrastructure directly, we have to instrument it by injecting custom probe code, which will then measure and collect the~required information. There are only a~few options which can be taken in order to inject custom code into the~agent system. The~MediationZone provides two options, apart from that we can also use \textit{Aspect Oriented Programming} (AOP) or similar code \textit{instrumentation tools}.

\subsubsection{MediationZone Plugin System}

The~first MZ option is to write code using the~provided DSL, called \textit{Analysis Programming Language} (APL). The~code in APL has to be written in the~GUI of MediationZone and is transpiled into Java bytecode on the~beginning of the~execution.

The~second MZ option is to write a~custom Java plugin. This plugin has to possess a~defined interface and structure and has to be registered in the~MediationZone. In addition to registration, it also has to be manually integrated (connected) within the~processing boxes or the~already-defined APL logic. Both previous options cannot be simply plugged in or out and therefore are not suitable for real-life data collection from the~PCRF.

\subsubsection{Aspect Oriented Programming}

AOP~\cite{aop-paper} is a~programming paradigm which is supposed to address the~separation of concerns. This is achieved by weaving custom code into predefined code locations, without modifying the~original code. The~most famous aspect implementation for Java is the~AspectJ framework.

AOP is centered around pointcuts, the~specification of locations where the~custom logic should be put. The~pointcut is composed of join points, which are the~specific places where the~advice code is put. The~joint points in the~classical AOP are quite restricted and generally available only for methods and alike.

\subsubsection{DiSL}

Also based on the~AOP concepts is \textbf{DiSL}~\cite{disl.org} -- a~domain-specific language for code instrumentation. Compared to AspectJ, DiSL allows a~more flexible definition of join points and generally provides more control over instrumentation. DiSL is an~academic project developed in cooperation between the~University of Lugano, Charles University in Prague and Shanghai Jiao Tong University.

Among the~previously outlined options, the~DiSL instrumentation framework seems to be the~only viable choice. Like AOP, DiSL instrumentation can be automated with no need for further actions or code modifications. We therefore implement the~data collection with DiSL. One small disadvantage of using DiSL is the~need to restart ECSA within the~MediationZone to apply the~instrumented code.
