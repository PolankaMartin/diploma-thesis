\chapter{Implementation}

There are two implementation parts of the~thesis. First one is implementation of the~\textbf{traffic generator} and second is implementation of the~\textbf{testing framework}. Traffic generator is used for generation of a traffic for the~PCRF server. It can generate messages for two Diameter interfaces -- Gx and Rx. The~traffic is generated based on the~concept of test profiles which includes scenarios, both were defined in Chapter~\ref{chapter-analysis}.

The~main function of testing framework is to combine, manage and prepare all things necessary to perform the~test profiles and, of course, perform them. The~test profiles are executed one by one once the~testing framework was started. There is also integrated instrumentation of the~MediationZone, that collects useful metrics. The~resulting statistics and graphs from the~metrics are outputted for all performed test profiles.

\section{Traffic Generator}

The~implementation of a~traffic generator for this thesis was named \textbf{traffirator} and its placed on \textit{GitHub} service under \textit{MIT} license. The~home of the~project is \url{https://github.com/Neloop/traffirator}, where all source codes can be found. A~user documentation of traffirator is present in Appendix~\ref{chapter-appendix-traffirator}.

Traffirator is written in the~\textit{Java} programming language with a~huge help of the~\textbf{jDiameter} library. The~choice of Java and the~jDiameter library was made with considerations to the~telecommunication provider, they are familiar with Java and were considering the~jDiameter library as a~base for testing anyway. The~build system for traffirator is maven with the~usage of a~custom repository definition for the~jDiameter dependency.

\subsection{jDiameter}

For the~Java programming language, there are only a~few generally accessible Diameter implementations. The~first one is included in \textit{Oracle Communications Converged Application Server} (OCCAS) which is a~framework used for building real-time applications mainly for IMS\footnote{IMS is a~separate component which ensures multimedia transfer and management within telecommunication provider network. IMS is based on the~IP protocol as opposed to older media transport systems.} network. In here, we can find API for Diameter Base Protocol alongside with Rf, Ro, and Sh Diameter applications. But this Diameter implementation is not available in form of a~library.

Another implementation is \textit{JavaDiameter}\footnote{\url{https://github.com/isj4/JavaDiameter}} -- very simple library which supports only Diameter Base Protocol. Every other application has to build upon that. The~author himself acknowledge that he is not a~Java programmer. By the~time of writing of the~thesis, the~project is abandoned, therefore not suitable for usage.

The~last usable implementation of Diameter in Java is \textit{jDiameter}\footnote{\url{https://github.com/RestComm/jdiameter}}. jDiameter is managed by the~RestComm company under the~\textit{AGPL-3.0} license. The~implementation of the~jDiameter supports the~Diameter Base Protocol and in addition, also many applications including Gx and Rx.

The~jDiameter library is quite heavy-weight, it is supposed to be run within \textit{JBoss} server and has many dependencies on the~JBoss utilities. If used with JBoss, jDiameter can be used in cluster mode with the~high-availability feature enabled. In addition, jDiameter has an~implementation of a~multiplexer which exposes the~Diameter stack to the~multiple listeners if necessary.

A~documentation of jDiameter is not very intuitive, but at least expose the~design of the~whole library. In addition to the documentation, there are few examples in the~repository of the~library, which are useful and can be used for further implementation.

\subsection{Configuration}

The~configuration of the~whole generator is unified into single \textbf{YAML}~\cite{yaml.org} configuration file. There are some settings for the~generator environment, such as the~number of threads and alike, and there is also definition of the~test profile which will be used for an~execution. The~configuration file has to be included as a command line argument during program startup.

Apart of the~YAML configuration, the~logging framework can be configured with the~separated configuration file. The~logging framework is \textit{log4j}~\cite{log4j.org} which is already used within jDiameter dependencies, so the~traffirator adopted it. The~configuration file is traditional and can be found in the~resources directory. The~structure of the~configuration is defined in the~appropriate logger documentation.

In traffirator, there is also a~third kind of configuration, the~configuration of jDiameter client stacks. For each used stack/application there has to be separate \textit{XML}~\cite{xml.org} configuration. This configuration specifies the~other side of the~connection, its IP address or domain name and the~application identification. There can also be set some internal values used within jDiameter for the~corresponding application, such as timeouts setup for base messages and threads setup. Traffirator expects configurations for Gx and Rx stacks on the~classpath, therefore in the~resources directory.

The~test profiles are implemented in traffirator in form of a~configuration. As stated, test profiles are specified in the~YAML configuration file which is provided on the~execution. Therefore, the~test profiles are dynamical and can be specified differently on every execution. There are example configurations in the~project which can be out-of-box used for traffic generation.

\subsection{Generator Logic}

The~concept of scenarios is implemented in-code and cannot be redefined by the~configuration. The~scenario templates are hard-coded and thus can be used only predefined ones. For now, real scenarios include only \textit{Call Center Employee}, \textit{Call Performance}, \textit{Classic User}, \textit{Travelling Manager} and \textit{Malfunctioning Cell-Phone}. In addition, there are two simple debugging scenarios.

Instances of the~scenarios are constructed using a~factory, which instantiates a scenario template. Scenario templates define the~whole scenario automaton and its nodes. In scenario nodes, there are defined steps, which has to be done when the~node is processed. The~scenario automaton is a~statical entity, which is shared amongst all instantiated scenarios of given type. The~instance of the~scenario contains the~reference to the~node, which is currently in processing. And there is also a~state of the~scenario, which can be accessed within scenario steps.

The~traffirator client logic is centered around \textit{test profiles} and \textit{scenarios}. On every execution, a~test profile is loaded and processed in a~given order. Whenever traffirator comes up to the~moment, where the~change of active scenarios is required, then it determines, if the~number of active scenarios of a~given type should be lower or higher and makes the~change. 

Internally traffirator contains \textit{scheduler} for high-level tasks, this ensures scalability and also full control over a threading model of an~application. The~main usage of the~scheduler is executing steps defined in the~scenarios instance. Traffirator holds a~list of references to the~active sessions and scenarios. Therefore, synchronization in form of locks takes place to ensure race-free execution, while working concurrently on lists of scenarios. Also, every scenario has its own lock which should be acquired while processing the~scenario instance.

Steps in a~particular scenario are executed one by one, logically there are only two types of steps, sending and receiving a~message. If there is a~new message, which ought to be sent, then the~sending itself is queued in the~scheduler. The~receival of the~messages is solved with listeners, interfaces for the~listeners are provided and called by the~jDiameter library. Received messages are also handed over to the~scheduler in form of a~task. After successful reception of the~message, if the~next step is sending, then it is scheduled for processing.

Additionally to the~received messages, there are also \textit{timeouts}, which might be defined in the~context of the~scenario. If the~next message in the~scenario is not received in the~time defined by the~timeout, then appropriate actions have to be done. Traffirator implements timeouts in the~way, that it means failure of the~whole scenario and there is created a~brand new scenario instance.

Traffirator provides also a~basic support for traffic \textit{statistics}. Statistics are sampled with a~period based on a~value given in the~YAML configuration file. A~filename used for the~statistics log is also defined in the~configuration. For the~time being the~statistics contain the~number of overall failures, the~number of timeouts, the~number of sent messages and the~number of received messages.

\section{Testing Framework}

Testing framework developed in the~context of this thesis is a~collection of shell scripts which sequentially perform all defined test profiles from Subsection~\ref{subsection-analysis-test-profiles} and needed maintenance between executions. Testing framework and other files connected to the~thesis are in the~file attachments. User documentation of the~framework can be found in Appendix~\ref{chapter-appendix-framework}.

\subsection{Test Profiles}

Introduced testing profiles and scenarios are based on the~real-life usage and expectancy. Based on the~defined test profiles, it would take 4 days to execute the~whole testing on a~single machine, which seems a~bit exaggerated. Not mentioning that the~server needed for testing the~real traffic might not be available at the~faculty.

Considering the~time consumption and the~lack of performance, the~solution can be to shorten the~times and lowering the~counts of active devices. For the~times, division by the~number \textbf{10} seems to be quite a~good choice, it is simple and the~resulting times are acceptable. The~whole execution after this shortening is done in roughly 10 hours.

After shortening of the~times by the~factor of 10, it seems only logical to lower the~numbers of the~devices also by dividing by \textbf{10}. But considering our possibilities in the~context of the~measuring hardware, the~resulting numbers may be lowered even more. Therefore, the~number 10 can be further revised, if needed.

Test profiles used for the~PCRF performance evaluation can be found in \texttt{pcrf-mff/real-scenarios/config} directory in appropriate configuration files.

\subsection{DiSL Instrumentation}

An~implementation of the~\textbf{DiSL} instrumentation contains three main entities. The~base instrumentation module which marks particular places of code where the~instrumented code should be put. And then two analysis classes which handle processing and logging of the~information collected during instrumentation.

There are two kinds of analysis, the~analysis of the~\textit{network interfaces} and the~analysis of the~PCRF \textit{Process} method. The~analysis of the~network interfaces includes various metrics usually regarding the~counts, for example, count of the~incomings messages, etc. The~analysis of the~processing method logs all processed messages and information about them.

DiSL works in the~following way. Instrumentation of the~code is done on the~separate server, which has to be run before the~application, which should be instrumented. The~definition of instrumentation has to be provided to the~DiSL server. The~instrumented application is then run with injected DiSL agent, which ensures that every loaded Java class is sent to the~DiSL server, possibly instrumented and sent back.

\subsubsection{Instrumentation and MediationZone}

When instrumenting MediationZone a~small problem emerged. The~problem was in the~usage of analysis modules. These modules are placed in separate Java classes, therefore, they are called within the~code which is injected in MZ. That means, the~analysis classes has to be accessible on the~classpath of MZ.

Unfortunately, only adding the~analysis classes to the~classpath does not solve the~problem. The~analysis uses internal classes of MZ to collect the~data from them. The~problem is that the~internal classes of MZ are loaded with different Java classloader than the~analysis, if it is placed on the~classpath. Therefore, when we try to use the~internal class of MZ within analysis we get “Class not found error” exception, because the~analysis is loaded with default classloader. And the~default classloader does not see classes loaded with its descendants, which are used within MZ.

The~solution to this problem is load the~analysis module of the~instrumentation with the~classloader used by MZ. Fortunately, there is a~concept of plugins in MZ, which can also be used to get custom Java jars into the~system. MediationZone uses this approach to inject Apache libraries. Therefore, after the~successful commit of the~analysis module in MZ, the~analysis started to see the~internal MZ classes. This is the~reason why the~instrumentation module and the~analysis modules are separated into different jar files.

\subsubsection{Instrumentation Output}

The~output files, where all the~logs from instrumentation are stored, are hardcoded in instrumentation classes. For the~time being, they are set up to be stored in the~\texttt{/tmp/pcrf-mff-out} directory. The~output directory is not configurable by some configuration file or option, because it is hard to define where the~configuration should be stored and how it should be read. The~only option would be to again hardcode the~path to the~configuration, which is kind of the~same as hardcoding the~directory for log files.

In conclusion, the~instrumentation is sampling and storing following values for both (Gx, Rx) network interfaces:

\begin{itemize}
    \conciseitemize
    \item \textit{Incoming} -- Count of incoming messages.
    \item \textit{Outgoing} -- Count of outgoing messages.
    \item \textit{IncomingDecoder} -- Count of incoming messages to the~decoder of the~Diameter messages.
    \item \textit{Rejected} -- Count of rejected messages by the~Diameter stack.
    \item \textit{TooBusy} -- Count of messages to which the~server responded with Too Busy Diameter message.
    \item \textit{InProcessing} -- Count of messages which are currently in processing.
    \item \textit{InQueue} -- Count of messages which are waiting for processing, therefore are in between the~receival and processing by the~Process method in the~PCRF server.
    \item \textit{Processed} -- Count of messages which were processed by the~main processing method of the~PCRF.
    \item \textit{Recv-Q} -- Number of bytes in the~receive queue of the~operating system.
    \item \textit{Send-Q} -- Number of bytes in the~send queue of the~operating system.
\end{itemize}

In addition to the~information from the~network interfaces, there are also data from the~Process method. These are monitored for each processed message:

\begin{itemize}
    \conciseitemize
    \item \textit{SessionId} -- Session identification of the~message.
    \item \textit{HbH} -- HopByHop Identifier of the~message.
    \item \textit{R/A} -- Request or Answer flag.
    \item \textit{CommandId} -- Command identification to which the~message belongs to.
    \item \textit{BeforeTime} -- Time which was measured right before the~Process method, it is defined in nanoseconds.
    \item \textit{AfterTime} -- Time measured right after the~Process method, it is defined in nanoseconds.
    \item \textit{ReqReceivedTime} -- Time of the~receival (in nanoseconds) of the~request, for the~received request, this is the~time, where the~message was received by the~Diameter stack in MZ. For the~received answer this time should be zero.
    \item \textit{ReqSentTime} -- The~exact point in time (in nanoseconds) when the~request was sent to the~client, non-zero only for the~received answer. The~MediationZone remembers sent requests and pair them with received answer, therefore it can populate this value.
    \item \textit{AnsReceivedTime} -- The~time when the~answer was received, defined in nanoseconds.
    \item \textit{AnsSentTime} -- Non-zero value only for the~received requests, it is the~time of sending the~answer to the~request back to the~client. The~value is defined in nanoseconds.
    \item \textit{PccTime} -- Duration of all methods concerning the~database, it can be lookups, saving the~data or committing the~transaction. It is defined in nanoseconds.
\end{itemize}

\subsection{Scripts}

All the~scripts and the~structure within the~file attachments of this thesis is shown in Figure~\ref{listing-framework-scripts}. All these scripts are used in the~testing framework during measurement of test profiles.

\begin{figure}[htb]
\dirtree{%
 .1 pcrf-mff.
 .2 disl.
 .3 make.sh.
 .3 post-run.sh.
 .3 pre-run.sh.
 .2 plots.
 .3 plot-all.r.
 .3 plots.sh.
 .3 utils.r.
 .2 real-scenarios.
 .3 run.sh.
}
\caption{Folder Structure of Testing Framework Scripts}
\label{listing-framework-scripts}
\end{figure}

The~measurement entry point is \texttt{pcrf-mff/real-scenarios/run.sh}, this script contains almost all logic regarding test profiles, preparation of MediationZone, Couchbase and traffirator. From the~base shell script, all others are called on execution.

The~logic of the DiSL instrumentation is located in the~\texttt{pcrf-mff/disl} directory and contains \texttt{make.sh} script, which compiles instrumentation classes. There are also \texttt{pre-run.sh} and \texttt{post-run.sh} scripts which should be run before a~startup of the~testing profile and after, respectively.

The~processing of the~outputted data to graphs is handled by scripts in \texttt{pcrf-mff/plots} directory. The~\texttt{plots.sh} script will find and hand over the~data to the~\texttt{plots.r} R script which will plot the~data into PDF files.

\subsubsection{Framework Flow}

Follows the~list of steps which has to be done during an~execution of the~whole measurement. Following list is also comprehensive description of all standalone measurement scripts. The~main script is \texttt{pcrf-mff/real-scenarios/run.sh}, where all execution begins:

\begin{itemize}
    \conciseitemize
    \item Load command line arguments, there are three of them and are mandatory. The~first one is taskset mask for traffirator, the~second one is taskset mask for PCRF and the~third one is the~amount of RAM which should be used for the~bucket in Couchbase.
    \item Initialize logging path, if it is not already initialized. The~logging path should be set to \texttt{/tmp/pcrf-mff-out}, as it is hardcoded into the~instrumentation modules.
    \item Compile traffirator which should be placed in \texttt{/opt/traffirator} folder. The~compilation is done with maven build system, which will package traffirator into jar file.
    \item Call the~\texttt{pcrf-mff/disl/make.sh} script:
    \begin{itemize}[topsep=0pt]
        \conciseitemize
        \item Find all Java source files concerning the~instrumentation and compile them using the~\texttt{javac} compiler.
        \item Compress compiled class files of the~instrumentation module to the~jar file, which will be given to the~classpath of the~DiSL server.
        \item Generate jar file based on compiled class files of the~analysis module, the~jar file will be committed as a~plugin to MediationZone.
        \item Create the~MediationZone plugin based on the~jar file generated from the~analysis module and commit it to MediationZone.
    \end{itemize}
    \item Define the~configuration files for traffirator and iterate over them.
    \begin{itemize}[topsep=0pt]
        \conciseitemize
        \item If the~iteration is consecutive and not the~first one, wait before doing any other steps. Basically, this wait is here to give a~server a~little pause to recover between executions.
        \item Call the~\texttt{pcrf-mff/disl/pre-run.sh} script:
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item Make a~backup copy of the~EC workers configuration located in file (\texttt{executioncontext.xml}), which is placed in configuration folder of MediationZone.
            \item Copy new configuration, which has options for JVM with included DiSL agent and instrumented code. The~configuration which is copied is placed in \texttt{pcrf-mff/disl/config}.
            \item Start the~DiSL instrumentation server.
            \item Restart ECSA which is executing the~PCRF logic, the~restart will load the~configuration file and apply JVM options to the~newly started ECSA server. The~restart is also run with the~taskset utility with the~mask given as a~command line argument.
        \end{itemize}
        \item Run a~single testing profile from the~defined configuration files:
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item Clean Couchbase from the~previous run by deleting the~whole bucket containing the~information from the~PCRF server.
            \item Enable the~dump and log workflows in MediationZone, which are used for logging in the~real PCRF.
            \item Just to be sure, stop and then start the~workflow containing the~PCRF logic in MediationZone. The~start of the~workflow will create a~new bucket in Couchbase, which was previously deleted.
            \item Setup the~Couchbase bucket to given amount of RAM memory and disable the~persistence to the~hard-drive to boost up the~performance.
            \item Start traffirator for the~specified configuration file with the~help of taskset. The~mask of the~taskset for traffirator should be set as the~command line argument for the~script.
            \item After the~execution of traffirator, stop the~workflow containing the~PCRF logic.
            \item Disable the~log and dump workflows in MediationZone.
            \item Dump the~database statistics from the~last day (Couchbase does not have custom granularity of statistics) in the \texttt{db-stats.json} file within the directory \texttt{/tmp/pcrf-mff-out/}.
        \end{itemize}
        \item Call the~\texttt{pcrf-mff/disl/post-run.sh} script:
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item Restore the~backup copy of the execution context configuration in the configuration folder of MZ.
            \item Restart ECSA which will load the~original configuration for the~execution context and start.
        \end{itemize}
        \item Call the~\texttt{pcrf-mff/plots/plots.sh} script:
        \begin{itemize}[topsep=0pt]
            \conciseitemize
            \item Locate all the~log files from the~execution of the~test profile, this includes following: both logs from instrumentation (Diameter stacks, Received messages), summary log from traffirator, statistics log from traffirator and the~statistics from Couchbase.
            \item All the~previously located log files copy to the~output directory which is located in \texttt{pcrf-mff/plots/out/\textless timestamp\textgreater}, where the~timestamp is current timestamp before the~copy action.
            \item Call the~\texttt{pcrf-mff/plots/plot-all.r} R script with the~Rscript application and give it output directory and all log files as arguments:
            \begin{itemize}[topsep=0pt]
                \conciseitemize
                \item The~R script will load all the~log files and plot them into PDF files to reflect various metrics.
            \end{itemize}
        \end{itemize}
    \end{itemize}
\end{itemize}
