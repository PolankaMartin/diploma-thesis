import glob
import csv
import os

def applynewline(col):
	if "\n" in col:
		return "\\textit{" + col.replace("\n", "}\\newline ")
	else:
		return col

def escapecol(col):
	return col.replace("_", "\\_")

def boldtext(col):
	if (col == "Session-Id" or
			col == "Origin-Host" or
			col == "Origin-Realm" or
			col == "Destination-Realm" or
			col == "CC-Request-Type" or
			col == "CC-Request-Number"):
		return "\\textbf{" + col + "}"
	return col

def groupedindentation(col):
	return col.replace("-> ", "$\\rightarrow$ ")

for filepath in glob.glob("*.csv"):
	texfilepath = "../" + os.path.splitext(filepath)[0] + ".tex"
	print("Processing {} -> {}".format(filepath, texfilepath))

	with open(filepath, 'rt') as csvfile, open(texfilepath, 'w') as texfile:
		texfile.write("\diametermsgtable{\n")
		content = csv.reader(csvfile, delimiter=';', quotechar='"')
		for row in content:
			first = True
			for col in row:
				if first:
					first = False
				else:
					texfile.write(" & ")
				texfile.write(applynewline(groupedindentation(boldtext(escapecol(col)))))
			texfile.write(" \\\\\\hline\n")
		texfile.write("}\n")
