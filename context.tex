\chapter{Context}
\label{chapter-context}

The~whole thesis is centered around the~\textbf{PCRF} component, which is part of the~IP Multimedia Subsystem (IMS). \textbf{IMS} is typically placed within \textbf{LTE} network or new generation networks in general, but can also be used in older telecommunication network architectures (2G, 3G). Therefore, IMS is a~separate component, which ensures multimedia transfer and management within telecommunication provider network. IMS, as the~name suggests, is based on the~\textbf{IP} protocol.

The~PCRF component is part of the~more general system called Policy and Charging Control (PCC), which is a~subpart of IMS. \textbf{PCC} is a~system used for charging users according to the~network usage and applying policies. Policies are rules applied to a~particular network transport, they can have a~form of QoS attributes or bandwidth allocation. The~PCC system contains three components -- \textit{PCRF}, \textit{PCEF}, and \textit{CSCF}.

The~PCRF component is connected with PCEF and CSCF, this connection is done with the~use of the~\textbf{Diameter} protocol. Diameter is the~new Authentication, Authorization, and Accounting (AAA) protocol built on the~successful predecessor RADIUS. AAA family of protocols generally manages an~access to the~network for end-user devices and they can also be used for tracking of the~network usage.

Considering the~previous description, the~user device, once it is connected to the~network, established a~communication with the~IMS network. This communication channel is prioritized and therefore it has to have assigned \textbf{bandwidth} and \textbf{QoS} attributes. The~enforcement of the~prioritization is done on the~PCEF component, which is also referred to as a~packet gateway. All the~communication from the~user device to the~IMS network goes through PCEF.

\textbf{PCEF} itself only enforces rules which come from PCRF, therefore, the~connection of the~device is signaled from the~PCEF to the~PCRF, which sends back the~right QoS attributes and bandwidth. The~initial established connection is held active for the~whole time, while the~user device is alive and all consecutive requests on the~IMS are done through this connection.

If a~call or video call is requested on the~user device, the~request goes to the~IMS, more specifically, to the~\textbf{CSCF} component, which manages the~calls. The~CSCF requests a~new transport channel through packet gateway dedicated to the~call, this request goes to PCRF. If PCRF decides to allow the~call, proper QoS attributes and bandwidth is sent to the~PCEF component, which then enforces them on the~established call.

\clearpage

\begin{sidewaysfigure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/lte-architecture.png}
\caption{LTE Architecture}
\label{figure-lte-architecture}
\captionsetup{font={footnotesize,it}}
\caption*{Source: \url{http://lteuniversity.com}}
\end{sidewaysfigure}

\clearpage

\section{Diameter Protocol}

The~\textbf{Diameter} protocol is a~successor of the~\textit{RADIUS}~\cite{RFC2865} protocol, also used for AAA -- Authentication, Authorization, and Accounting. The~name of the~protocol is a~word pun, the~diameter is double the~radius, which should express its superiority. As the~predecessor, RADIUS was taken as a~basis for the~Diameter protocol and considerable effort was put into making the~Diameter similar to the~RADIUS, which should make a~transition between these two protocols easier. The~Diameter protocol is defined as an~internet standard managed by the~IETF, in RFC 6733~\cite{RFC6733} which obsoletes the~original RFC 3588~\cite{RFC3588}.

The~Diameter protocol extends and modifies RADIUS extensively. Both of them has support for additional content in the~messages with the~use of Attribute-Value Pairs (\textbf{AVPs} for short). In addition to RADIUS, there is support for capabilities negotiation between two Diameter peers. Another feature is peer discovery and autoconfiguration, which in many cases replaces manual configuration. Diameter is also transmitted over \textit{TCP} and not \textit{UDP}, which was the~protocol of choice for RADIUS. And last but not least, Diameter supports agents such as proxies or redirects. In this thesis, we will focus on the~format of the~Diameter messages and applications which use Diameter, other advanced features like agents are out of the~scope of the~thesis and will not be discussed.

RFC 6733 defines only the~Diameter base protocol. On top of that, custom applications can define their own message structure and content. An~application can be defined either in an~internet standard by IETF, or by the~3GPP organization, or it can be defined in an~entirely custom way. The~official applications and their assigned identifications are managed by IANA in a~corresponding document. The~default identification is number \textit{0} and represents Diameter common message, which can be used for capabilities negotiation, etc.

\subsection{Base Protocol}

The~base protocol defines the~mandatory message header that has to be included in every message, and, optionally, additional AVPs. The~header is 20 bytes long and is depicted in Figure~\ref{figure-diameter-format}. The~whole structure of the~Diameter message is discussed in more detail:

\begin{figure}[htb]
\centering
\includegraphics{./img/drawio/diameter-format.pdf}
\caption{Diameter Message Format}
\label{figure-diameter-format}
\end{figure}

\begin{itemize}
	\conciseitemize
	\item \textit{Version} -- Indicates the~version of Diameter, at the~time being, this is number 1.
	\item \textit{Message Length} -- a~3-byte number which gives the~length of the~message, including the~header and the~padded AVPs.
	\item \textit{Command Flags} -- Eight flags which determine further characteristics of the~message, four are used, and four are reserved.
	\begin{itemize}[topsep=0pt]
		\conciseitemize
		\item \textit{Request} -- If set, the~message is a~request, otherwise, it is an~answer.
		\item \textit{Proxiable} -- The~message can be proxied, relayed, or redirected.
		\item \textit{Error} -- Set only if the~message contains a~protocol error.
		\item \textit{Potentially Retransmitted Message} -- Set in case the~message is resent and not yet acknowledged.
	\end{itemize}
	\item \textit{Command Code} -- Again a~3-byte number which should be used for distinguishing between message types.
	\item \textit{Application-ID} -- a~4-byte number used to identify the~Diameter application.
	\item \textit{Hop-by-Hop Identifier} -- 4-byte unsigned integer used for pairing requests and answers. The~sender has to assign a~unique identifier when sending the~message, and the~answer should contain the~same number. The~Hop-by-Hop Identifier must be unique within a~connection.
	\item \textit{End-to-End Identifier} -- An~unsigned 4-byte integer which is used for detection of duplicate messages. The~identifier in the~answer has to match the~identifier in the~request. The~End-to-End Identifiers have to remain unique for at least 4 minutes, including across reboots.
	\item \textit{AVPs} -- Attribute-Value Pairs contain further data in the~message. AVPs are defined by the~particular Diameter applications.
\end{itemize}

\subsubsection{Command Codes}

Each Diameter message has to include a~Command Code, used for determining the~actions which should be taken at the~server or the~client after the~arrival of that message. The~base protocol defines some basic commands that can be used regardless of the~application. All commands have variants for requests and answers but the~Command Code identification remains the~same for both messages. The~difference is only in their abbreviations and in the~format of the~messages which will be introduced further. Same as the~Diameter application identifiers, the~Command Codes are also managed by IANA.

In Table~\ref{table-diameter-commands}, there is an~excerpt from the~RFC summarized in a~more structured way. The~descriptions are rewritten in the~thesis context, in contrast to the~ones in the~specification. The~table depicts Diameter base commands and their major characteristics.

\begin{table}[!htb]
\centering
\begin{tabular}{|p{23mm}|p{28mm}|p{11mm}|p{65mm}|}
\hline
\textbf{Command Name} & \textbf{Abbreviation} & \textbf{Code} & \textbf{Description} \\\hline
Abort-Session & ASR/ASA & 274 & A~Diameter server may send this request if for example there is no more credit for the~session. Client that receives ASR should stop the~session and not use it in further messages. \\\hline
Accounting & ACR/ACA & 271 & Support for accounting is included in the~Diameter base protocol, thus special Diameter messages are defined for accounting. \\\hline
Capabilities-Exchange & CER/CEA & 257 & After initial establishment of connection two connected peers should exchange their capabilities using these messages. \\\hline
Device-Watchdog & DWR/DWA & 280 & Used for transport failure detection, these messages can be sent periodically to determine if the~other node is still alive. \\\hline
Disconnect-Peer & DPR/DPA & 282 & A~Diameter node can decide to end the~connection using this message. Can contain the~disconnect reason. \\\hline
Re-Auth & RAR/RAA & 258 & A~server initiated re-authentication or re-authorization for particular session. Specific meaning is defined in particular Diameter application. \\\hline
Session-Termination & STR/STA & 275 & Sent by the~Diameter client when the~session is no longer valid, which tells the~server that it can release the~resources bound to the~session. \\\hline
\end{tabular}
\caption{Diameter Base Protocol -- Commands}
\label{table-diameter-commands}
\end{table}

Every Diameter command has to specify the~structure of the~message. The~format used for this purpose is also defined in RFC 6733, it is called the~\textit{Command Code Format} (CCF). CCF is based on the~\textit{Augmented Backus-Naur Form} defined in RFC 5234~\cite{RFC5234}. An~example of this format can be seen in the~code example~\ref{listing-cer-abnf}, which defines the~CER message and its required attributes. This definition is directly taken from RFC 6733.

\begin{figure}[htb]
\begin{lstlisting}[frame=single,caption={CER Messages ABNF Definition},label=listing-cer-abnf]
<CER> ::= < Diameter Header: 257, REQ >
                   { Origin-Host }
                   { Origin-Realm }
                1* { Host-IP-Address }
                   { Vendor-Id }
                   { Product-Name }
                   [ Origin-State-Id ]
                 * [ Supported-Vendor-Id ]
                 * [ Auth-Application-Id ]
                 * [ Inband-Security-Id ]
                 * [ Acct-Application-Id ]
                 * [ Vendor-Specific-Application-Id ]
                   [ Firmware-Revision ]
                 * [ AVP ]
\end{lstlisting}
\end{figure}

\subsubsection{AVPs}

In addition to the~header, each Diameter message contains data structured as Attribute-Value Pairs (AVPs). These values can be used to transfer information about AAA and also capabilities data, routing data, etc. The~Diameter messages are something like containers, there is a~basic structure, but the~data transferred through the~messages can be arbitrary. Theoretically, they even do not have to correspond with the~usage of AAA protocols and can be used for other purposes. AVPs have specified format depicted in Figure~\ref{figure-avp-format} and contain the~following items:

\begin{figure}[htb]
\centering
\includegraphics{./img/drawio/avp-format.pdf}
\caption{Diameter AVP Format}
\label{figure-avp-format}
\end{figure}

\begin{itemize}
	\conciseitemize
	\item \textit{AVP Code} -- The~AVP Code combined with the~\textit{Vendor-ID} item identifies the~AVP in a~unique way. Like the~Command Codes, AVP Codes are managed by IANA, at least the~standardized ones.
	\item \textit{AVP Flags} -- Eight flags of the~AVP, three are defined, five reserved.
	\begin{itemize}[topsep=0pt]
		\conciseitemize
		\item \textit{Vendor-Specific} -- When the~flag is set, the~\textit{Vendor-ID} field is provided by the~sender.
		\item \textit{Mandatory} -- If the~flag is set then the~receiver has to parse the~value correctly upon reception. Also defines the~AVP as mandatory for usage in the~particular Diameter application.
		\item \textit{P} -- Flag reserved for the~future usage, to ensure end-to-end security. At the~time being it should be set to 0.
	\end{itemize}
	\item \textit{AVP Length} -- An~unsigned 3-byte integer defining the~length of the~AVP, including AVP header and data.
	\item \textit{Vendor-ID} -- Flag present only if the~\textit{Vendor-Specific} flag is set. It contains a~unique number which identifies the~vendor who registered the~AVP. Also defined by IANA in \textit{SMI Network Management Private Enterprise Codes} document~\cite{iana-enterprise-numbers}.
	\item \textit{Data} -- The~value of the~particular AVP, can be zero length.
\end{itemize}

Every AVP has to have the~type of its value defined, this type is not stored inside the~message. The~diameter node (server/client) has to know the~type of the~particular AVP based on the~given AVP Code. The~Diameter Base Protocol specifies \textit{Basic AVP Data Formats}. Custom formats are possible but they should be derived from the~standard provided basic types. The~list of basic types follows:

\begin{itemize}
	\conciseitemize
	\item \textit{OctetString} -- Arbitrarily long string
	\item \textit{Integer32} -- 32-bit signed value
	\item \textit{Integer64} -- 64-bit signed value
	\item \textit{Unsigned32} -- 32-bit unsigned value
	\item \textit{Unsigned64} -- 64-bit unsigned value
	\item \textit{Float32} -- single precision floating point number as described by IEEE 754-1985~\cite{IEEE30711}
	\item \textit{Float64} -- double precision floating point number as described by IEEE 754-1985
	\item \textit{Grouped} -- Grouped AVP data format is the~way the~AVPs can include the~sequence of another AVPs, therefore, AVPs supports nesting
\end{itemize}

Based on the~basic data formats, the~Diameter Base Protocol also defines \textit{Common Derived AVP Data Formats} which could be useful for further usage. A~list of the~derived formats follows, the~description of the~types is omitted and can be found in the~appropriate RFC.

\begin{itemize}
	\conciseitemize
	\item \textit{Address} -- Derived from OctetString
	\item \textit{Time} -- Derived from OctetString
	\item \textit{UTF8String} -- Derived from OctetString
	\item \textit{DiameterIdentity} -- Derived from OctetString
	\item \textit{DiameterURI} -- Derived from OctetString
	\item \textit{Enumerated} -- Derived from Integer32
	\item \textit{IPFilterRule} -- Derived from OctetString
\end{itemize}

\section{LTE}

\textbf{LTE} stands for Long-term Evolution, a~telecommunication standard which supersedes the~3G\footnote{ITU standard IMT-2000} networks. Formally LTE does not comply with the~\textbf{4G}\footnote{ITU standard IMT-Advanced} networks specification, due to high-speed expectancy for 4G networks, but it was retroactively accepted as the~new generation network. At this time, the~LTE is commonly advertised as the~4G network by the~telecommunication providers. LTE was first introduced by 3GPP in Release 8 of the~standard specifications. In the~following chapter, we will cover only a~few important areas which are needed for better understanding of the~thesis.

LTE is a~major step forward in the~telecommunication industry. Up to this point, the~architecture of the~network had to handle two types of communication. One based on circuit switching, such as calls and messages, and the~other based on packet switching, used for data transfer. Keeping two separate and even contradictory network approaches had, of course, high maintenance costs, thus the~main change in the~LTE architecture was the~use of only one network approach, based on \textbf{packet switching}. This little change had major impact on the~whole telecommunication provider network architecture and led to massive changes and an~introduction of new network components.

The~LTE architecture is defined by the~3GPP~TS~23.002~\cite{3gpp.23.002} and is composed of the~following main components: \textit{E-UTRAN}, \textit{EPC}, and \textit{IMS}. \textbf{E-UTRAN} is an~acronym for \textit{Evolved Universal Mobile Telecommunications System Terrestrial Radio Access Network} and it is specified by the~3GPP~TS~36.401~\cite{3gpp.36.401}. E-UTRAN is a~new access network designed for the~purposes of LTE and has higher throughput than the~previous telecommunication standards. Another part of LTE is \textbf{EPC}, which stands for \textit{Evolved Packet Core} -- the~core part of the~telecommunication network which manages the~user equipment and appropriate communication.

The~changes of the~network architecture are most visible in the~EPC and IMS parts of LTE. IP Multimedia Subsystem (\textbf{IMS}) is the~system that ensures the~transport of the~calls, messages, and multimedia over the~IP network and it is contained within LTE architecture. Both of EPC and IMS rely only on packet switching, which in some parts simplifies the~overall architecture significantly. EPC manages the~user equipment, its authentication, authorization, and data transport through the~border gateways. The~overall LTE architecture is depicted on Figure~\ref{figure-lte-architecture}.

In the~overall LTE architecture, the~PCRF component is placed within EPC and with other elements like the~PCEF and the~CSCF, it is part of the~more general system called Policy and Charging Control (PCC). The~\textbf{PCC} is a~general system used for charging users according to the~network usage and applying policies. In the~context of LTE, PCC was defined by 3GPP~TS~23.203~\cite{3gpp.23.203}. In this standard, we can find description of PCRF and also basic definitions of the~interfaces between the~network elements.

\subsection{PCRF}

The~\textbf{PCRF} is an~acronym of \textit{Policy and Charging Rules Function} and in the~context of the~PCC, it is responsible for determining the~right QoS attributes and bandwidth based on given user information. The~PCRF is connected with two other components, through the~\textbf{Gx} interface with the~PCEF and through the~\textbf{Rx} interface with the~CSCF, both will be described later.

The~PCRF is a~newly introduced component in the~LTE telecommunication network. In cooperation with the~\textit{PCEF} and \textit{CSCF}, PCRF ensures that calls and messages, which had dedicated circuit-switching-based network in the~previous network standards, are prioritized before any other data transfer. Higher priority is provided by the~\textit{QoS-over-IP} protocol, which has to be supported in the~EPC and the~IMS part of the~new telecommunication network. The~main QoS parameter is the~bandwidth dedicated to the~transfer. There is an~expression used for dedicated transfer -- the~\textbf{bearer}.

The~whole PCRF functionality is focused around bearers. When the~user device initially connects to the~network, the~PCEF notifies the~PCRF about this. On the~PCEF the~default bearer is created and used for communication between the~user device and the~network of the~telecommunication provider. This bearer is used whenever the~user requests some action from the~IMS, like call or message. In the~former case, the~CSCF will request a~dedicated call bearer from the~PCRF. If the~processing on the~PCRF is successful, then the~PCEF is notified about a~new bearer, which should be established at the~packet gateway. In addition, the~PCRF also handles updates of the~used codec during the~call, which might or might not change the~bearer bandwidth. When the~user device disconnects, the~PCRF is notified about this again by the~PCEF.

PCRF can implement relatively advanced logic based on the~provided user data. In the~reference architecture, the~PCRF is connected to the~Subscription Profile Repository (SPR). The~\textbf{SPR} is a~database of all user devices registered in the~network of the~telecommunication provider. Based on this repository, the~PCRF can decide what policies and QoS attributes will be used for the~user device. In addition, the~PCRF has to handle and store the~information about active sessions, this can be managed by the~SPR or a~different kind of persistent cache.

PCRF communicates through two interfaces, Gx and Rx. The~Gx interface faces the~PCRF and uses session identifiers to distinguish individual user devices, while the~Rx interface connects to the~CSCF and uses session identifiers to refer to calls or messages. Both interfaces within the~PCRF server use Diameter protocol for network communication.

\subsection{PCEF}

Policy and charging enforcement function (\textbf{PCEF}) is usually implemented in the~packet gateway (\textit{PGW}) component. Packet gateway is the~entry point for all IP based communication originating at the~user device. The~function of the~PCEF is managing bearers and enforcing that all communication has attributes which were requested by the~PCRF. The~packet gateway has information about the~connection and disconnection of all user devices and also about updates of the~device state. PCEF shares all this information with the~PCRF, which takes appropriate actions. For example, during connection of the~new device, a~session is created and stored in the~PCRF for further reference and processing.

\subsection{CSCF}

\textbf{CSCF} is an~abbreviation for \textit{Call Session Control Function}, it refers to a~component placed within the~IMS. It can be split into three subsystems: \textit{Proxy-CSCF}, \textit{Interrogating-CSCF}, and \textit{Serving-CSCF}. All of these together are used to process SIP messages from the~user devices and for controlling the~session information about devices and calls.

If there is a~request for a~new call from the~user device, CSCF will handle this through the~Rx interface, connected to the~PCRF. The~CSCF will request a~dedicated bearer for the~incoming or outgoing call and the~PCRF should resend proper policies to the~PCEF. Another example of communication between CSCF and PCRF is updating a~call codec, this is also handled in the~IMS and the~information about the~change is handed over to the~PCRF. The~PCRF might or might not update the~bearer managed by the~PCEF.

\subsection{Diameter Applications}

The~following chapter contains the~description and the~specification of the~newly defined Diameter applications used for the~purposes of the~PCRF and LTE networks.

\subsubsection{Diameter Gx Application}

As previously stated, specialized applications can be defined for practical use of Diameter. For the~PCRF and its interfaces, two Diameter applications are defined: \textit{Gx} and \textit{Rx}. Both applications define new message types and their structure. There are also defined new AVPs which can be used within messages.

The~Diameter \textbf{Gx} application is defined by 3GPP in the~3GPP~TS~29.212~\cite{3gpp.29.212} specification and connects the~PCEF with the~PCRF. It is based on the~Diameter base protocol and also uses some existing commands from the~\textit{Diameter Credit Control Application} standardized by RFC 4006~\cite{RFC4006}. IANA assigned number to the~Gx application is \textit{16777238}, this number is used in all Gx-specific messages sent over the~interface. The~vendor identifier common to all newly created AVPs is set to \textit{10415}. Commands which are defined for the~Gx interface are described in Table~\ref{table-diameter-gx-commands}. In addition to new commands, base control messages for capabilities exchange and such can also be used.

\begin{table}[!htb]
\centering
\begin{tabular}{|p{23mm}|p{28mm}|p{11mm}|p{65mm}|}
\hline
\textbf{Command Name} & \textbf{Abbreviation} & \textbf{Code} & \textbf{Description} \\\hline
Credit-Control & CCR/CCA & 272 & Control messages are used by end-user devices to manage specific connectivity -- in particular, a~CCR/CCA pair is used for example to reserve the~base bearer for control communication between the~phone and the~network. The~CCR/CCA messages can be also used for the~base bearer termination or for update the~base bearer attributes. \\\hline
Re-Auth & RAR/RAA & 258 & Re-Auth messages are used for management of call bearers, as such they are similar to the~CC messages. They are used for the~allocation of the~call bearer, they can also be used for update of the~call bearer QoS attributes. And they are also used for the~bearer termination. \\\hline
\end{tabular}
\caption{Diameter Gx Application -- Commands}
\label{table-diameter-gx-commands}
\end{table}

\subsubsection{Diameter Rx Application}

The~Diameter \textbf{Rx} application is specified by 3GPP in the~3GPP~TS~29.214~\cite{3gpp.29.214} document and connects the~PCRF with the~CSCF. It is based on the~Diameter base protocol with the~Diameter \textit{Network Access Server Requirements} (NASREQ) application, defined by RFC 4005~\cite{RFC4005}. The~assigned number of the~application is \textit{16777236}, the~vendor identifier valid for all new AVPs is \textit{10415}, connected to the~3GPP organization. Commands supported by the~Rx interface can be found in Table~\ref{table-diameter-rx-commands}. In addition to them, base control messages for capabilities exchange and such from the~Diameter base protocol can be used.

\begin{table}[!htb]
\centering
\begin{tabular}{|p{23mm}|p{28mm}|p{11mm}|p{65mm}|}
\hline
\textbf{Command Name} & \textbf{Abbreviation} & \textbf{Code} & \textbf{Description} \\\hline
AA & AAR/AAA & 265 & Sent by the~CSCF in order to manage a~call session. This usually means starting a~new call or update of an~existing one. The~AAR message contains particular request for the~call bearer attributes, like QoS or bandwidth. The~PCRF can send a~bearer installation request to the~PCEF. The~PCRF immediately responds with the~appropriate answer and does not wait for the~answer coming back from the~PCEF. \\\hline
Re-Auth & RAR/RAA & 258 & Re-Auth messages are not sent by the~provided implementation of the~PCRF server. But it might be sent by different PCRF implementations. It is a~PCRF-originated request, it can be an~indication of change of attributes of the~call session. This can include re-authentication or re-authorization of the~call session. \\\hline
Session-Termination & STR/STA & 275 & The~STR request is sent by CSCF when the~call session is no longer valid. It tells the~PCRF that the~session in a~session cache can be freed. After successful session termination, an~answer is sent by the~PCRF. \\\hline
Abort-Session & ASR/ASA & 274 & Abort session request is sent in case a~call session was terminated and PCRF received this information from PCEF. Therefore, the~call session termination has to be propagated to CSCF. Abort session usually happens when the~user device lost connection during a~call. \\\hline
\end{tabular}
\caption{Diameter Rx Application -- Commands}
\label{table-diameter-rx-commands}
\end{table}

\section{PCRF Implementation}
\label{section-pcrf-implementation}

As stated in \nameref{chapter-introduction}, the~thesis topic was motivated by the~situation at certain major Czech telecommunication provider. In the~context of the~cooperation, a~real-life implementation of the~PCRF server component was provided to be instrumented and experimented with. 

The~production PCRF server runs in a~virtual machine environment and includes a~persistent cache for sessions. The~provider recommended using at least three machines for the~PCRF evaluation purposes. On two machines are the~PCRF servers, one in active mode and one idle. The~third machine and both PCRF servers also hold the~replicated and distributed persistent cache for sessions.

\subsection{MediationZone}

The~implementation of the~PCRF component itself is based on an~agent system developed by the~\textbf{DigitalRoute} company. The~agent system, called \textbf{MediationZone}, is proprietary and works as a~framework with its own graphical user interface. The~GUI allows visual programming in the~form of connecting processing boxes. The~business logic of the~boxes can be predefined or custom-defined with the~help of a~domain-specific language (DSL). The~agent system is implemented in Java and the~DSL is Java-like, but without classes and class methods. In addition, custom plugins for the~agent system can be written in Java and imported into the~system as an~alternative custom-defined logic.

MediationZone is in divided into three distinct components: The~\textit{Platform}, The~\textit{Execution Context Standalone}, and The~\textit{Execution Context}. The~Platform is the~main component that handles all subcomponents and manages the~whole system. The~Execution Context Standalone (ECSA) and Execution Context (EC) components are workers that execute work assigned by the~Platform. Both of them can run as standalone applications on separate servers and have to be connected to the~Platform. ECSA is able to continue to work even if the~connection with the~Platform was lost, but EC will stop its execution during connection problems.

The~processing logic of the~system is represented by \textit{workflows} that contain the~individual processing boxes. There can be multiple processing units (workflows) in the~application. The~communication between the~boxes uses the~concept of queues, which are transparent for the~boxes and are handled by the~system itself. This includes even the~communication between different machines in the~case of the~standalone ECSA worker. Every box handles the~given input and can have multiple outputs which are sent to other boxes or network interfaces.

The~MediationZone is presented as a~system for real-time processing with out-of-the-box support for various protocols and storage drivers. Important is the~support for the~\textit{Diameter} protocol and the~\textit{Couchbase} storage. Couchbase is used in the~PCRF as a~persistent storage for the~sessions. The~logic of the~PCRF itself is implemented in one of the~processing boxes using the~DSL language. The~DSL logic is transpiled into Java code on execution. The~whole processing workflow of the~PCRF can be seen in Figure~\ref{figure-pcrf-wfl}, where the~Process box contains the~custom logic which implements the~PCRF behavior.

\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{./img/pcrf-wfl.png}
\caption{PCRF Workflow in MediationZone}
\label{figure-pcrf-wfl}
\end{figure}

\subsection{High-availability}

High availability of the~PCRF server is solved on the~virtual machine level -- there is a~backup virtual machine in the~same datacenter which is in an~idle state. The~same setup also runs in a~backup datacenter in a~different geolocation and can be used in case of a~blackout in the~primary datacenter. There is also the~possibility to use high availability support provided by the~MediationZone itself, but considering previous assumptions, this feature is not needed and not enabled in the~provider network.

\subsection{Sessions Storage}

The~cache for sessions representing user devices or calls is backed by the~\textbf{Couchbase}~\cite{couchbase.com} NoSQL distributed database. Couchbase is a~merged product of \textit{memcached}~\cite{memcached.org} and \textit{CouchDB}~\cite{couchdb.org}. Memcached is a~distributed memory object caching system and in the~Couchbase, it serves as a~high-performance data cache. CouchDB is a~classical NoSQL database for storing JSON objects, which is used as a~persistent storage. On top of these two components, Couchbase adds replication, sharding management, and fault resilience, all of this achieved by having multiple nodes organized in a~cluster.

\subsection{Provided Tests}

Two kinds of tests were provided with the~PCRF implementation -- \textit{regression tests} and \textit{performance tests}. The~regression tests are implemented as a~separate workflow in the~MediationZone that reads custom-defined configuration files containing test scenarios. These tests are relatively simple and are testing only the~proper behavior and the~message flow of the~PCRF. They are not suitable for performance testing and also their definition is quite limited.

The~second kinds of tests are performance tests, these are implemented using the~seagull~\cite{seagull.net} multi-protocol traffic generator. \textbf{Seagull} is well known in the~telecommunication industry as it aims to generate workload for the~IMS. Seagull supports multiple network protocols including Diameter and thus is suitable for testing of PCRF. Seagull is presented as a~high-performance traffic generator written in the~C++ language and can be used for functional, load, endurance, stress, performance, and benchmark tests.

The~definition of the~test cases in seagull is wrapped in \textit{scenarios}, which can represent for example a~user device. One execution of seagull can use only one scenario, which can be executed multiple times in a~sequence or concurrently. In addition, messages can be sent and received to and from multiple network interfaces.

As a~significant limitation, the~list of actions defined in a~scenario has to be processed one by one. This means that it is not possible to define reaction to two messages received concurrently or in a~different order than that defined in the~scenario. This is a~problem for PCRF testing because messages on the~Rx and Gx interfaces sent by the~PCRF can be received in any order. If seagull encounters this situation, it will report test failure. As a~workaround, separate instances of seagull could be run, each handling one interface, but that gives rise to problems with synchronization and control over the~scenarios, to which seagull has no useful solution.

The~provided performance tests used two separate instances of the~seagull generator, but in order to keep at least some synchronization, the~tests had to be very simple and straightforward. Therefore, they are not suitable for real-life scenarios testing, which can be sometimes quite complex and hard to synchronize. Considering this, writing new PCRF specific generator seems like the~only viable option.

\section{Related Work}

Web document~\cite{effort.com-diameter} from the~EFORT company presents comprehensive overview of Diameter Base Protocol. Regarding Diameter there is also a~bit more official tutorial~\cite{ietf.org-diameter} presented by the~IETF organization. Overview about seagull traffic generator can be found in web document~\cite{hp-seagull} written by the~HP company.

LTE and related technologies are very popular in a~modern telecommunication world, therefore, there are multiple articles and documents regarding this topic. There is a~couple of web articles~\cite{3gpp.org-lte}~\cite{3gpp.org-epc} from the~3GPP organization which give overall description about LTE and its core components. Netmanias.com is a~portal aimed to network and communication which introduce many useful web articles~\cite{netmanias-pcrf-component}~\cite{netmanias-pcrf-rx} about PCRF and its position within LTE network. There is also large article~\cite{netmanias-comprehensive-pcrf} at Netmanias.com about PCRF which covers its history, placement within EPC and also its function and architecture.
